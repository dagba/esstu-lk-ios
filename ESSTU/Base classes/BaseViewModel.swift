import Foundation
import RxSwift

class BaseViewModel {
    let alertMessage = PublishSubject<Error>()
    let isLoading    = Variable<Bool>(false)
}
