import UIKit

class ChatMessageTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        transform = CGAffineTransform.init(scaleX: 1, y: -1)
        selectionStyle = .none
        
    }

}
