import UIKit

class SeparatorLessTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Remove insets visible
        let width = bounds.width / 2
        separatorInset = UIEdgeInsets.init(top: 0, left: width, bottom: 0, right: width)
    }

}
