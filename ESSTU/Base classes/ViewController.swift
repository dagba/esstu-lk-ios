import UIKit
import NVActivityIndicatorView

class ViewController: UIViewController {
    
    public var spinner: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setlinearBarConstraints()
//        setNavBarClear()
    }
    
    /// Func definitions
    private func setlinearBarConstraints() {
        spinner = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.height * 0.08, height: UIScreen.main.bounds.height * 0.08),
                                            type: .circleStrokeSpin,
                                            color: #colorLiteral(red: 0.2703937138, green: 0.4843118631, blue: 0.5647058824, alpha: 1),
                                            padding: nil)
        
        spinner.center = CGPoint(x: view.bounds.maxX/2, y: view.bounds.maxY/2)
        view.addSubview(spinner)
    }
    
    public func setNavBarClear() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
    }
    
    public func showAlert(with error: Error) {
        var errorDescription = error.localizedDescription
        if let err = error as? APIError {
            switch err {
            case .wrongAuthUrl(let x):
                errorDescription = x
            case .gettingStudyWeekfailed(let x):
                errorDescription = x
            case .mapFailed(let x):
                errorDescription = x
            case .wrongLoginPassword(let x):
                errorDescription = x
            }
        }
        let alert = UIAlertController(title: nil, message: errorDescription, preferredStyle: .alert)
        // сделать кнопку перезайти в аккаунт если mapFailed
        let action = UIAlertAction(title: "OK", style: .destructive) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    public func showAlert(with description: String) {
        let alert = UIAlertController(title: nil, message: description, preferredStyle: .alert)
        // TODO: сделать кнопку перезайти в аккаунт если mapFailed
        let action = UIAlertAction(title: "OK", style: .destructive) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

