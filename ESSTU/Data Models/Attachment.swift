import Foundation
import ObjectMapper

private enum AttachmentKey: String {
        case id     = "id"
        case type   = "type"
        case fileId = "fileCode"
        case name   = "fileName"
        case size   = "fileSize"
}

struct Attachment: Mappable {
    var id     : String?
    var type   : String?
    var fileId : String?
    var name   : String?
    var size   : String?
    
    init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    mutating func mapping(map: Map) {
        id       <- map[AttachmentKey.id.rawValue]
        type     <- map[AttachmentKey.type.rawValue]
        fileId   <- map[AttachmentKey.fileId.rawValue]
        name     <- map[AttachmentKey.name.rawValue]
        size     <- (map[AttachmentKey.size.rawValue], Helper.FileSizeTransform())
    }
}
