import Foundation
import ObjectMapper

enum DialogKey: String {
    case peerId         = "peerId"
    case lastMessageId  = "lastMessageId"
    case unreadCount    = "unreadCount"
    case notifySettings = "notifySettings"
    case type           = "type"
}

enum DialogType: String {
    case announce     = "ANNOUNCEMENT"
    case chat         = "CHAT"
    case dialogue     = "DIALOGUE"
}

class Dialog: Mappable {
    var peerId         : String?
    var lastMessageId  : String?
    var unreadCount    : Int?
    var notifySettings : Bool?
    var type           : String?
    var preview        : Message?
    var user           : User?
    var chatDescription: ChatDescription?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        peerId          <- (map[DialogKey.peerId.rawValue], Helper.IntToStringTransform())
        lastMessageId   <- (map[DialogKey.lastMessageId.rawValue], Helper.IntToStringTransform())
        unreadCount     <- map[DialogKey.unreadCount.rawValue]
        notifySettings  <- map[DialogKey.notifySettings.rawValue]
        type            <- map[DialogKey.type.rawValue]
    }
}
