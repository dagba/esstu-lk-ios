import Foundation
import ObjectMapper

fileprivate struct GroupKey {
    static let name = "name"
    static let participantsCount = "usersCount"
}

struct Group: Mappable {
    var name : String?
    var participantsCount: Int?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        name              <- map[GroupKey.name]
        participantsCount <- map[GroupKey.participantsCount]
    }
}
