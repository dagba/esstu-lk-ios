import Foundation
import ObjectMapper

private enum MessageKey: String {
    case id          = "id"
    case fromId      = "from"
    case peerId      = "peerId"
    case text        = "message"
    case date        = "date"
    case countViews  = "views"
    case attachments = "attachments"
    case reply       = "replyToMsgId"
}

struct Message: Mappable {
    var id          : String?
    var fromId      : String?
    var fromUser    : User?
    var peerUser    : User?
    var peerId      : String?
    var text        : String?
    var date        : Date?
    var countViews  : Int?
    var reply       : String?
    var attachments : [Attachment]?
    
    init?(map: Map) { }
    init() {
        
    }
    
    mutating func mapping(map: Map) {
        id          <- (map[MessageKey.id.rawValue], Helper.IntToStringTransform())
        fromId      <- map[MessageKey.fromId.rawValue]
        peerId      <- (map[MessageKey.peerId.rawValue], Helper.IntToStringTransform())
        text        <- map[MessageKey.text.rawValue]
        date        <- (map[MessageKey.date.rawValue], Helper.MillisecondsDateTransform())
        countViews  <- map[MessageKey.countViews.rawValue]
        attachments <- map[MessageKey.attachments.rawValue]
        reply       <- (map[MessageKey.reply.rawValue], Helper.IntToStringTransform())
    }
}

class ChatDescription: Mappable {
    var id                : String?
    var type              : String?
    var title             : String?
    var participantsCount : Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id                  <- (map[MessageKey.id.rawValue], Helper.IntToStringTransform())
        type                <- map[DialogKey.type.rawValue]
        title               <- map["name"]
        participantsCount   <- map["participantsCount"]
    }
}
