import Foundation
import ObjectMapper

struct Room: Mappable {
    
    public var coordinates : [CGPoint] = []
    public var floor       : Int       = 0
    public var height      : Int       = 0
    public var lastPoint   : CGPoint   = CGPoint.zero
    public var mapImageId  : String    = ""
    public var width       : Int       = 0
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
        coordinates <- (map[RoomMapKey.coordinates.rawValue], RoomMapCoordinatesTransform())
        floor       <- map[RoomMapKey.floor.rawValue]
        height      <- map[RoomMapKey.height.rawValue]
        lastPoint   <- (map[RoomMapKey.lastPoint.rawValue], LastPointTransform())
        mapImageId  <- map[RoomMapKey.mapImageId.rawValue]
        width       <- map[RoomMapKey.width.rawValue]
    }
    
}

private enum RoomMapKey: String {
    case coordinates = "coords"
    case floor       = "floor"
    case height      = "height"
    case lastPoint   = "lastPoint"
    case mapImageId  = "photo"
    case width       = "width"
}

private final class LastPointTransform: TransformType {
    public typealias Object = CGPoint
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Object? {
        guard let coords = value as? String else { return CGPoint.zero }
        
        let unsortedList = coords.split(whereSeparator: { $0 == " " }).map({ CGFloat(Double($0) ?? 0.0) })
        if let x = unsortedList.first, let y = unsortedList.last {
            return CGPoint(x: x, y: y)
        }
        
        return CGPoint.zero
    }
    
    public func transformToJSON(_ value: Object?) -> JSON? {
        
        return nil
    }
}

private final class RoomMapCoordinatesTransform: TransformType {
    public typealias Object = [CGPoint]
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Object? {
        guard let coords = value as? String else { return [CGPoint]() }
        
        let unsortedList = coords.split(whereSeparator: { $0 == " " }).map({ CGFloat(Double($0) ?? 0.0) })
        var sortedCoords = [CGPoint]()
        
        for (index, coord) in unsortedList.enumerated() {
            var lastIndex = sortedCoords.count == 0 ? 0 : sortedCoords.count - 1
            
            if (index + 1) % 2 != 0 {
                sortedCoords.append(CGPoint.zero)
                lastIndex = sortedCoords.count == 0 ? 0 : sortedCoords.count - 1
                sortedCoords[lastIndex].x = coord
            } else {
                sortedCoords[lastIndex].y = coord
            }
        }
        
        return sortedCoords
    }
    
    public func transformToJSON(_ value: Object?) -> JSON? {
        
        return nil
    }
}
