import Foundation
import ObjectMapper

struct Subject: Mappable {
    var name            : String?
    var dayNumber       : Int?
    var weekNumber      : Int?
    var lessonNumber    : Int?
    var lessonType      : String?
    var classroomNumber : String?
    var teacherInfo     : String?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        name            <- map[SubjectKey.name]
        dayNumber       <- map[SubjectKey.dayNumber]
        weekNumber      <- map[SubjectKey.weekNumber]
        lessonNumber    <- map[SubjectKey.lessonNumber]
        lessonType      <- map[SubjectKey.lessonType]
        classroomNumber <- map[SubjectKey.classroomNumber]
        teacherInfo     <- map[SubjectKey.teacherInfo]
    }
}

struct SubjectKey {
    static let name            = "subject.name"
    static let dayNumber       = "weekday"
    static let weekNumber      = "weekNumber"
    static let lessonNumber    = "lessonNumber"
    static let lessonType      = "burdenType"
    static let classroomNumber = "auditorium"
    static let teacherInfo     = "teacher"
    static let labName         = "LABOR_ZAN"
    static let lectureName     = "LECIYA"
    static let practiceName    = "RAKTIAKA"
}

