import Foundation
import ObjectMapper

private struct UserKey {
    static let userType     = "userType"
    static let firstName    = "firstName"
    static let lastName     = "lastName"
    static let middleName   = "patronymic"
    static let userId       = "userId"
    static let accessToken  = "access_token"
    static let refreshToken = "refresh_token"
    static let description  = "information"
    static let id           = "id"
    static let gender       = "sex"
    static let photo        = "photo"
}

struct MainUser: Mappable {
    var userType     : String?
    var firstName    : String?
    var middleName   : String?
    var lastName     : String?
    var refreshToken : String?
    var accessToken  : String?
    var userId       : String?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        userType     <- map[UserKey.userType]
        firstName    <- map[UserKey.firstName]
        middleName   <- map[UserKey.middleName]
        lastName     <- map[UserKey.lastName]
        refreshToken <- map[UserKey.refreshToken]
        accessToken  <- map[UserKey.accessToken]
        userId       <- map[UserKey.userId]
    }
}

struct User: Mappable {
    var firstName    : String?
    var middleName   : String?
    var lastName     : String?
    var userId       : String?
    var description  : String?
    var gender       : String?
    var photo        : String?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        firstName   <- map[UserKey.firstName]
        middleName  <- map[UserKey.middleName]
        lastName    <- map[UserKey.lastName]
        userId      <- map[UserKey.id]
        description <- (map[UserKey.description], Helper.UserDescriptionTransform())
        gender      <- map[UserKey.gender]
        photo       <- map[UserKey.photo]
    }
}
