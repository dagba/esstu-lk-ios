import Foundation
import ObjectMapper

struct Helper {
    
    class FileSizeTransform: TransformType {
        public typealias Object = String
        public typealias JSON = Int
        
        public init() {}
        
        public func transformFromJSON(_ value: Any?) -> Object? {
            if let size = value as? Int {
                var _size = Double(size)
                
                if _size < 1024 { return "1 KB" }
                _size = _size/1000
                if _size > 1000 {
                    _size = _size / 1000
                    if _size > 1000 { return "\(String(format: "%.f", _size/1000)) GB" }
                    return "\(String(format: "%.f", _size)) MB"
                } else {
                    return "\(String(format: "%.f", _size)) KB"
                }
            }
            
            return nil
        }
        
        public func transformToJSON(_ value: Object?) -> JSON? {
            if let value = value {
                return Int(value)
            }
            return nil
        }
    }
    
    class IntToStringTransform: TransformType {
        public typealias Object = String
        public typealias JSON = Int
        
        public init() {}
        
        public func transformFromJSON(_ value: Any?) -> Object? {
            if let interval = value as? Int {
                return String(interval)
            } else {
                return value as? String
            }
        }
        
        public func transformToJSON(_ value: Object?) -> JSON? {
            if let date = value {
                return Int(date) ?? 1
            }
            return nil
        }
    }
    
    class MillisecondsDateTransform: TransformType {
        public typealias Object = Date
        public typealias JSON = Int
        
        public init() {}
        
        public func transformFromJSON(_ value: Any?) -> Object? {
            if let interval = value as? Int {
                return Date(timeIntervalSince1970: Double(interval/1000))
            }
            return nil
        }
        
        public func transformToJSON(_ value: Object?) -> JSON? {
            if let date = value {
                return Int(date.timeIntervalSince1970 * 1000)
            }
            return nil
        }
    }
    
    class TimestampDateTransform: TransformType {
        public typealias Object = Date
        public typealias JSON = Int
        
        public init() {}
        
        public func transformFromJSON(_ value: Any?) -> Object? {
            if let interval = value as? Int {
                return Date(timeIntervalSince1970: Double(interval))
            }
            return nil
        }
        
        public func transformToJSON(_ value: Object?) -> JSON? {
            if let date = value {
                return Int(date.timeIntervalSince1970)
            }
            return nil
        }
    }
    
    
    class UserDescriptionTransform: TransformType {
        public typealias Object = String
        public typealias JSON = String
        
        public init() {}
        
        public func transformFromJSON(_ value: Any?) -> Object? {
            guard let text = value as? String else { return nil }
            
            return text.replacingOccurrences(of: ";", with: "\n")
        }
        
        public func transformToJSON(_ value: Object?) -> JSON? {
            return value?.replacingOccurrences(of: "\n", with: ";")
        }
    }
}
