import UIKit

extension NSObject {
    
    // Return name of class
    static var typeName: String {
        return String(describing: self.self)
    }
    
}
