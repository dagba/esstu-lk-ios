import UIKit

extension String {
    
    /// if cleanedText have no characters return nil
    var cleanedText: String? {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? nil : self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
