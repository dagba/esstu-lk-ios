import UIKit

class AttachmentTableViewCell: BaseDialogTableViewCell {
    
    @IBOutlet weak var nameLabel     : UILabel!
    @IBOutlet weak var sizeLabel     : UILabel!
    @IBOutlet weak var iconImageView : UIImageView! {
        didSet {
            iconImageView.layer.cornerRadius  = 10
            iconImageView.layer.masksToBounds = true
        }
    }
    
}
