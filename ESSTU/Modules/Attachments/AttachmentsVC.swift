import UIKit
import RxSwift
import RxCocoa
import NVActivityIndicatorView

class AttachmentsViewController: ViewController, UIDocumentInteractionControllerDelegate {
    private let disposeBag = DisposeBag()
    
    /// MARK: - Properties and UI connections
    
    public  var attachments: [Attachment]?
    private let fileViewer    = UIDocumentInteractionController()
    private var filesToDelete = [URL]()
    private let loadingSpinner = UIActivityIndicatorView()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate   = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView()
        }
    }
    
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never
        fileViewer.delegate = self
        spinner = nil
        setLoadingSpinner()
    }
    
    /// Remove downloaded files
    deinit {
        for url in filesToDelete {
           try? FileManager.default.removeItem(at: url)
        }
    }
    
    /// UIDocumentInteractionControllerDelegate
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.navigationController ?? self
    }
    
}


/// MARK: - DataSource+Delegate methods
extension AttachmentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = attachments?.count else { return 0 }
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard !loadingSpinner.isAnimating else {
            return
        }
        guard let fileId = attachments?[indexPath.row].fileId, let fileName = attachments?[indexPath.row].name else {
            self.showAlert(with: "Файл не найден")
            return
        }
        
        loadingSpinner.startAnimating()
        
        APIManager.shared.downloadFileWith(url: NetworkKey.baseDownloadingURL + fileId, fileName: fileName)
            .subscribe(onNext: { [weak self] (url) in
                self?.filesToDelete.append(url)
                self?.showFileWithPath(path: url.path, fileName: fileName)
                }, onError: { [weak self] (error) in
                    self?.showAlert(with: error)
                    self?.loadingSpinner.stopAnimating()
            })
            .disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AttachmentTableViewCell.typeName, for: indexPath) as! AttachmentTableViewCell
        cell.nameLabel.text = attachments?[indexPath.row].name
        cell.sizeLabel.text = attachments?[indexPath.row].size
        
        return cell
    }
}

/// MARK: - Func definitions

extension AttachmentsViewController {
//    private func share(url: URL, fileName: String) {
//        documentInteractionController.url = url
//       documentInteractionController.uti = "public.data, public.content"
//        documentInteractionController.name = fileName
//        documentInteractionController.presentOptionsMenu(from: view.frame, in: view, animated: true)
//    }
    
    private func showFileWithPath(path: String, fileName: String) {
        let isFileFound: Bool = FileManager.default.fileExists(atPath: path)
        if isFileFound {
            fileViewer.url = URL(fileURLWithPath: path)
            fileViewer.name = fileName
            DispatchQueue.main.async { [weak self] in
                if self?.viewIfLoaded?.window != nil {
                    self?.fileViewer.presentPreview(animated: true)
                }
                self?.loadingSpinner.stopAnimating()
            }
        }
    }
    
    private func setLoadingSpinner() {
        loadingSpinner.hidesWhenStopped = true
        loadingSpinner.color = .red
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: loadingSpinner)
    }
    
}
