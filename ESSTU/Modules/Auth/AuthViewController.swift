import UIKit
import RxSwift
import RxCocoa
import Material

class AuthViewController: ViewController {
    private var viewModel  = AuthViewModel()
    private let disposeBag = DisposeBag()
    private var router: AuthViewRouter!
    
    /// MARK: - Properties and UI Outlets
    
    @IBOutlet private weak var loginTextField    : UITextField!
    @IBOutlet private weak var passwordTextField : UITextField!
    @IBOutlet private weak var loginButton       : UIButton!
    @IBOutlet private weak var restoreButton     : UIButton!
    @IBOutlet private weak var logoImageView     : UIImageView!
    @IBOutlet private weak var loginSpinner      : UIActivityIndicatorView!
    
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(0, forKey: "START_PAGE_INDEX") // Назначает стартовой страницей - экран расписания
        setUpKeyboardTapDismiss()
        setTextFields()
        setUpLoginButton()
        setUpRestoreButton()
        bindViewModel()
        router = AuthViewRouter(base: self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setUpLogoImageView()
    }
    
}

/// MARK: - Func definition
extension AuthViewController {
    
    private func bindViewModel() {
        let loginValidation = Observable.combineLatest(loginTextField.rx.text, passwordTextField.rx.text) { (login, password) -> Bool in
            guard let login = login, let password = password else { return false }
            return login.count > 1 && password.count > 1
        }
        
        loginValidation
            .map { $0 ? 1 : 0.5 }
            .bind(to: loginButton.rx.alpha)
            .disposed(by: disposeBag)
        
        loginValidation
            .bind(to: loginButton.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)
        
        let hidesKeyboard = Observable.of(loginTextField.rx.controlEvent(.editingDidEndOnExit).asObservable(),                           passwordTextField.rx.controlEvent(.editingDidEndOnExit).asObservable()).merge()
        
        hidesKeyboard
            .subscribe { [unowned self] (_) in
                if !self.passwordTextField.isEditing {
                    self.passwordTextField.becomeFirstResponder()
                } else {
                    self.passwordTextField.resignFirstResponder()
                }
            }
            .disposed(by: disposeBag)
        
        loginButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.passwordTextField.resignFirstResponder()
                self?.loginTextField.resignFirstResponder()
                self?.viewModel.loginWith(username: self?.loginTextField.text ?? "", password: self?.passwordTextField.text ?? "")
            })
            .disposed(by: disposeBag)
        
        restoreButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.showAlert(with: "Вы можете получить логин и пароль в библиотеке ВСГУТУ")
            })
            .disposed(by: disposeBag)
        
        viewModel.alertMessage
            .bind { [weak self] (error) in
                self?.showAlert(with: error)
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .bind(onNext: { isLoading in
                if isLoading {
                    self.loginSpinner.startAnimating()
                    self.loginButton.setTitle("", for: UIControl.State.normal)
                } else {
                    self.loginSpinner.stopAnimating()
                    self.loginButton.setTitle("Войти", for: UIControl.State.normal)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.user
            .bind(onNext: { [weak self] (usr) in
                self?.router.showMainTabBar()
            })
            .disposed(by: disposeBag)
    }
    
    private func setTextFields() {
        (loginTextField as! TextField).dividerActiveColor        = #colorLiteral(red: 1, green: 0.9607843137, blue: 0.007843137255, alpha: 1)
        (passwordTextField as! TextField).dividerActiveColor     = #colorLiteral(red: 1, green: 0.9598122011, blue: 0.006951592421, alpha: 1)
        (passwordTextField as! TextField).placeholderActiveColor = #colorLiteral(red: 1, green: 0.9598122011, blue: 0.006951592421, alpha: 1)
        (loginTextField as! TextField).placeholderActiveColor    = #colorLiteral(red: 1, green: 0.9598122011, blue: 0.006951592421, alpha: 1)
        (loginTextField as! TextField).placeholderNormalColor    = #colorLiteral(red: 0.8392156863, green: 0.9019607843, blue: 0.9568627451, alpha: 1)
        (passwordTextField as! TextField).placeholderNormalColor = #colorLiteral(red: 0.8411616657, green: 0.9033913493, blue: 0.9570200586, alpha: 1)
        (passwordTextField as! TextField).textColor              = #colorLiteral(red: 1, green: 0.9598122011, blue: 0.006951592421, alpha: 1)
        (loginTextField as! TextField).textColor                 = #colorLiteral(red: 1, green: 0.9598122011, blue: 0.006951592421, alpha: 1)
    }
    
    private func setUpLogoImageView() {
        logoImageView.layer.shadowColor = UIColor.white.cgColor
        logoImageView.layer.shadowRadius = 8.0
        logoImageView.layer.shadowOpacity = 0.25
        logoImageView.layer.masksToBounds = false
    }
    
    private func setUpLoginButton() {
        loginButton.alpha = 0.5
        loginButton.isUserInteractionEnabled = false
        loginButton.layer.cornerRadius = 6
        loginButton.backgroundColor = .white
        loginButton.setTitleColor(UIColor.red, for: .normal)
    }
    
    private func setUpRestoreButton() {
        restoreButton.titleLabel?.fontSize = 30
        restoreButton.titleLabel?.minimumScaleFactor = 0.5
        restoreButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    private func setUpKeyboardTapDismiss() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(keyboardDismiss))
        tap.numberOfTouchesRequired = 1
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardDismiss() {
        view.endEditing(true)
    }
}
