import Foundation
import RxSwift

class AuthViewModel: BaseViewModel {
    private let disposeBag = DisposeBag()
    public  let user       = PublishSubject<MainUser>()
    
    override init() {
        super.init()
    }
    
    public func loginWith(username: String, password: String) {
        isLoading.value = true
        APIManager.shared.rx_authentication(username: username, password: password)
            .subscribe(onNext: { [weak self] (usr) in
                (UIApplication.shared.delegate as! AppDelegate).getToken() // запрашивает токен и регистрирует на esstu сервере
                self?.isLoading.value = false
                self?.user.onNext(usr)
                }, onError: { [weak self] (error) in
                    self?.isLoading.value = false
                    self?.alertMessage.onNext(error)
            })
            .disposed(by: disposeBag)
    }
    
}
