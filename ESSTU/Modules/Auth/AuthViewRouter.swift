import UIKit
import RxSwift
import RxCocoa

protocol AuthViewRouterProtocol: class {
    func showMainTabBar()
}

class AuthViewRouter: AuthViewRouterProtocol {
    
    weak var baseViewController: UIViewController?
    
    init(base: UIViewController) {
        self.baseViewController = base
    }
    
    public func showMainTabBar() {
        let vc = Assembler.createMainViewController()
        vc.modalPresentationStyle = .overFullScreen
        baseViewController?.present(vc, animated: true)
    }
}
