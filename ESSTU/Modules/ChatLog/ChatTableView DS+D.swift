import UIKit
import SafariServices

extension ChatViewController: UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {
    
    // MARK: - CONSTANTS

    private static let SECTION_HEIGHT = CGFloat(25.0)
    
    // MARK: - Method delegate
    
    fileprivate func sectionViewWith(_ title: String) -> UIView {
        let titleFont = UIFont.preferredFont(forTextStyle: .caption2)
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: ChatViewController.SECTION_HEIGHT))
        titleLabel.backgroundColor = .white
        titleLabel.transform = CGAffineTransform.init(scaleX: 1, y: -1)
        titleLabel.textAlignment = .center
        titleLabel.textColor = #colorLiteral(red: 0.1753054604, green: 0.4077930662, blue: 0.4197248712, alpha: 0.2578066796)
        titleLabel.alpha = 1
        titleLabel.font = titleFont
        titleLabel.text = title
        
        return titleLabel
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionModel = viewModel.sectionList[section]
        return sectionModel.countRow
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard viewModel.sectionList[indexPath.section].type != .read else {
            return 20
        }
        
        let sectionModel = viewModel.sectionList[indexPath.section]
        let isMessageOrSenderNameCell = indexPath.row == sectionModel.countRow - 1
        let section = viewModel.showReadCell ? indexPath.section - 1 : indexPath.section
        let msg = viewModel.list.value[section]
        
        if !sectionModel.isMessageEmpty && isMessageOrSenderNameCell {
            return UITableView.automaticDimension
        }
        
        if sectionModel.type == .user {
            if sectionModel.isMessageEmpty {
                if msg.attachments?[indexPath.row].type?.contains("image") ?? false {
                    return 240
                } else {
                    return 56
                }
            } else if !isMessageOrSenderNameCell {
                if msg.attachments?[indexPath.row].type?.contains("image") ?? false {
                    return 198
                } else {
                    return 56
                }
            }
        } else {
            if sectionModel.isMessageEmpty && isMessageOrSenderNameCell {
                return 23
            } else if !isMessageOrSenderNameCell {
                if msg.attachments?[indexPath.row].type?.contains("image") ?? false {
                    return 240
                } else {
                    return 56
                }
            }
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard viewModel.sectionList[section].type != .read else {
            return nil
        }
        
        let title = viewModel.sectionList[section].title
        let sectionView = sectionViewWith(title)
        
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        if let cell = tableView.cellForRow(at: indexPath) as? ChatAttachmentCell, let url = cell.fileURL {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            config.barCollapsingEnabled = true
            let safariVC = SFSafariViewController(url: url, configuration: config)
            safariVC.preferredControlTintColor = .red
            safariVC.delegate = self
            present(safariVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard viewModel.sectionList[section].type != .read else {
            return 0.0
        }
        return ChatViewController.SECTION_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard viewModel.sectionList[indexPath.section].type != .read else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ReadCell.typeName, for: indexPath) as! ReadCell
            cell.configure(msg: viewModel.list.value.first)
            return cell
        }
        
        let sectionModel = viewModel.sectionList[indexPath.section]
        let section = viewModel.showReadCell ? indexPath.section - 1 : indexPath.section
        let msg = viewModel.list.value[section]
        let row = indexPath.row
        
        if row == (sectionModel.countRow - 1) && !sectionModel.isMessageEmpty {
            if sectionModel.type == .user {
                if let replyId = msg.reply, let replyMsg = viewModel.list.value.first(where: { $0.id == replyId }) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: MessageReplyUserCell.typeName, for: indexPath) as! MessageReplyUserCell
                    let i = viewModel.list.value.index { replyMsg.id == $0.id }
                    cell.configure(msg: msg, replyMsg: replyMsg) { [unowned self] in
                        self.tableView.scrollToRow(at: IndexPath(row: sectionModel.countRow - 1, section: i ?? 0), at: .top, animated: true)
                    }
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: MessageUserCell.typeName, for: indexPath) as! MessageUserCell
                cell.configure(msg: msg)
                return cell
                
            } else if sectionModel.type == .sender {
                if let replyId = msg.reply, let replyMsg = viewModel.list.value.first(where: { $0.id == replyId }) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: MessageReplySenderCell.typeName, for: indexPath) as! MessageReplySenderCell
                    let i = viewModel.list.value.index { replyMsg.id == $0.id }
                    cell.configure(msg: msg, replyMsg: replyMsg) { [unowned self] in
                        self.tableView.scrollToRow(at: IndexPath(row: sectionModel.countRow - 1, section: i ?? 0), at: .top, animated: true)
                    }
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: MessageSenderCell.typeName, for: indexPath) as! MessageSenderCell
                cell.configure(msg: msg)
                return cell
                
            }
        } else {
            if row == (sectionModel.countRow - 1) && sectionModel.type == .sender && sectionModel.isMessageEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: SenderNameCell.typeName, for: indexPath) as! SenderNameCell
                var senderDisplayName: String {
                    let first = "\(msg.fromUser?.lastName ?? "") \(msg.fromUser?.firstName ?? "")"
                    let second = "\(msg.fromUser?.firstName ?? "") \(msg.fromUser?.middleName ?? "")"
                    return msg.fromUser?.userId?.first == "e" ? second : first
                }
                cell.configure(with: senderDisplayName)
                return cell
            }
            if let attachment = msg.attachments?[row] {
                if attachment.type?.contains("image") ?? false {
                    let cell = tableView.dequeueReusableCell(withIdentifier: ChatImageViewCell.typeName, for: indexPath) as! ChatImageViewCell
                    cell.configure(attachment: attachment,senderType: sectionModel.type)
                    cell.showImage = { [weak self] image in
                        let imageShowVC = ImageShowViewController(nibName: ImageShowViewController.typeName, bundle: nil)
                        imageShowVC.setImage(image)
                        self?.present(imageShowVC, animated: true, completion: nil)
                    }
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: ChatAttachmentCell.typeName, for: indexPath) as! ChatAttachmentCell
                    cell.configure(attachment: attachment,senderType: sectionModel.type)
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true)
    }
    
}
