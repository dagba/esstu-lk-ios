import UIKit
import RxSwift
import RxCocoa
import Material
import NVActivityIndicatorView
import AVFoundation
import MobileCoreServices
import Photos
import GrowingTextView

class ChatViewController: ViewController {
    private let disposeBag = DisposeBag()
    let viewModel = ChatViewModel()
    
    /// MARK: - Properties and UI connections
    
    @IBOutlet weak var tableView: UITableView!
    private var imagePickerController = UIImagePickerController()
    @IBOutlet weak var sendSpinner: UIActivityIndicatorView!
    @IBOutlet weak var attachmentButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var attachmentCount: UILabel!
    @IBOutlet weak var participantsBarButton: UIBarButtonItem!
    @IBOutlet weak var bottomInputView: UIView!
    @IBOutlet weak var inputTextView: GrowingTextView!
    @IBOutlet weak var bottomInputViewHeightConsctraint: NSLayoutConstraint!
    
    public var photoImage: UIImage?
    
    /// MARK: - UI Actions
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        setInputTextView()
        setUpTableView()
        setRightBarButtons()
        setUpNavBar()
        bindViewModel()
        viewModel.fetchMessageLog()
        setUpAttachmentCount()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        navigationItem.largeTitleDisplayMode = .never
    }
    
}

/// MARK: - InputTextView delegate methods

extension ChatViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        let height = bottomInputViewHeightConsctraint.constant + (height - textView.bounds.height)

        UIView.animate(withDuration: 0.1) { [unowned self] in
            if height < 50 {
                self.bottomInputViewHeightConsctraint.constant = 50
            } else {
                self.bottomInputViewHeightConsctraint.constant = height
            }
            self.view.layoutIfNeeded()
        }
    }
}

/// MARK: - Func definition

extension ChatViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIDocumentPickerDelegate {
    
    fileprivate func bindViewModel() {
        
        participantsBarButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                let userListVC = Assembler.createUsersListViewController() as! UserListViewController
                userListVC.peerId = self.viewModel.peerId
                self.navigationController?.pushViewController(userListVC, animated: true)
            })
            .disposed(by: disposeBag)
        
        viewModel.alertMessage
            .bind { [weak self] (error) in
                self?.showAlert(with: error)
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .bind(onNext: { [weak self] isLoading in
                if isLoading {
                    self?.view.makeToastActivity(.center)
                } else {
                    self?.view.hideToastActivity()
                    self?.tableView.reloadData()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.keyboardHeightObservable
            .bind(onNext: { (height) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame.origin.y = -1.0 * height
                    self.lockSendButtonAnimation()
                })
            })
            .disposed(by: disposeBag)
        
        sendButton.rx.tap
            .bind { [unowned self] _ in
                let clearText = self.inputTextView.text?.cleanedText
                if clearText != nil || self.viewModel.capturedImages.count > 0 {
                    self.viewModel.sendMessage(with: clearText)
                    self.attachmentCount.isHidden = true
                    self.attachmentCount.text = "0"
                }
            }
            .disposed(by: disposeBag)
        
        inputTextView.rx.text
            .orEmpty
            .filter { $0.count < 2 && !self.viewModel.isMessageUploading.value }
            .bind { [weak self] _ in
                self?.lockSendButtonAnimation()
            }
            .disposed(by: disposeBag)
        
        viewModel.isMessageUploading.asObservable()
            .skip(1)
            .bind { [weak self] (isLoading) in
                if isLoading {
                    self?.isUploadingAnimation()
//                    self?.tableView.beginUpdates()
//                    if self!.viewModel.showReadCell {
//                        self?.tableView.insertSections([1], with: UITableView.RowAnimation.fade)
//                        if self!.viewModel.numberOfAddedSections > 1 {
//                            self?.tableView.insertSections([0], with: UITableView.RowAnimation.fade)
//                        }
//                    } else {
//                        self?.tableView.insertSections([0], with: UITableView.RowAnimation.fade)
//                    }
//                    self?.tableView.endUpdates()

                } else {
                    self?.hideKeyboard()
                    self?.isUploadingAnimation()
                    self?.lockSendButtonAnimation()
                }
            }
            .disposed(by: disposeBag)
        
        attachmentButton.rx.tap
            .bind { [weak self] in
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                alert.addAction(UIAlertAction(title: "Камера", style: UIAlertAction.Style.default, handler: { action in
                    self?.setUpImagePicker(UIImagePickerController.SourceType.camera)
                }))
                
                alert.addAction(UIAlertAction(title: "Фото", style: UIAlertAction.Style.default, handler: { action in
                    self?.setUpImagePicker(UIImagePickerController.SourceType.photoLibrary)
                }))
                
                alert.addAction(UIAlertAction(title: "Сброс", style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                    self?.attachmentCount.isHidden = true
                    if (self?.viewModel.capturedImages.count ?? 2) > 0 {
                        self?.viewModel.capturedImages.removeAll()
                        self?.attachmentCount.text = "0"
                    }
                }))
                
                let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertAction.Style.cancel) { action in
                    alert.dismiss(animated: true, completion: nil)
                }
                cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
                alert.addAction(cancelAction)
                alert.modalPresentationStyle = .formSheet
                self?.present(alert, animated: true, completion: nil)
            }
            .disposed(by: disposeBag)
        
    }
    
    /// UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
        var imageURL: URL?
        if #available(iOS 11.0, *) {
            if let URL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.imageURL)] as? URL {
                imageURL = URL
            }
        } else {
            if let Url = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
                imageURL = Url
            }
        }
        let imageName = imageURL?.lastPathComponent ?? "/\(DateFormatter().string(from: Date())).jpg"
        isUploadingAnimation()
        viewModel.uploadImage(image: image as! UIImage, name: imageName)
            .subscribe(onNext: { [weak self] (code) in
                self?.inputTextView.becomeFirstResponder()
                self?.isUploadingAnimation()
            })
            .disposed(by: disposeBag)
        if attachmentCount.isHidden {
            attachmentCount.isHidden = false
            attachmentCount.text = "\(Int(attachmentCount.text ?? "0")! + 1)"
        } else {
            attachmentCount.text = "\(Int(attachmentCount.text ?? "0")! + 1)"
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func hideKeyboard() {
        inputTextView.text?.removeAll()
        inputTextView.resignFirstResponder()
    }
    
    fileprivate func setUpTableView() {
        tableView.keyboardDismissMode = .none
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.transform = CGAffineTransform.init(scaleX: 1, y: -1)
        tableView.estimatedRowHeight = 56
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    fileprivate func setUpNavBar() {
        navigationController?.view.backgroundColor = .white
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.red,
                                                                   NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)]
    }
    
    fileprivate func showImagePicker(sourceType: UIImagePickerController.SourceType, _ imagePickerController: UIImagePickerController) {
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true, completion: nil)
    }
    
    fileprivate func setUpAttachmentCount() {
        attachmentCount.cornerRadiusPreset = .cornerRadius3
        attachmentCount.layer.masksToBounds = true
    }
    
    fileprivate func setRightBarButtons() {
        if navigationController?.viewControllers[navigationController!.viewControllers.count - 2] is DialogueViewController {
            navigationItem.rightBarButtonItems?.removeAll()
            let photoImageView = UIImageView(image: photoImage)
            photoImageView.image = photoImage
            let width: CGFloat = UIScreen.main.bounds.width >= 375 ? 33.0 : 25.0
            let widthConstraint = NSLayoutConstraint(item: photoImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: width)
            let heightConstraint = NSLayoutConstraint(item: photoImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: width)
            photoImageView.addConstraints([widthConstraint, heightConstraint])
            photoImageView.layer.cornerRadius = width/2
            photoImageView.contentMode = .scaleAspectFill
            photoImageView.clipsToBounds = true
            let item = UIBarButtonItem(customView: photoImageView)
            navigationItem.rightBarButtonItem = item
        }
    }
    
    fileprivate func setUpImagePicker(_ sourceType: UIImagePickerController.SourceType) {
        imagePickerController.modalPresentationStyle = .popover
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        imagePickerController.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        if authStatus == AVAuthorizationStatus.denied {
            let alertA = UIAlertController(title: "Нет доступа к Камере",
                                           message: "Вы можете дать доступ к Камере в настройках.",
                                           preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertA.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Настройки", style: .default, handler: { _ in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Finished opening URL
                    })
                }
            })
            alertA.addAction(settingsAction)
            
            present(alertA, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.showImagePicker(sourceType: sourceType, self.imagePickerController)
                    }
                }
            })
        } else {
            showImagePicker(sourceType: sourceType, imagePickerController)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func isUploadingAnimation() {
        if !sendSpinner.isAnimating {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: { [weak self] in
                self?.sendButton.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
                self?.sendButton.isHidden = true
                self?.sendButton.alpha = 0
                self?.sendSpinner.startAnimating()
            })
        } else {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: { [weak self] in
                self?.sendSpinner.stopAnimating()
                self?.sendButton.isHidden = false
                if !(self?.inputTextView.isFocused ?? true) {
                    self?.sendButton.isUserInteractionEnabled = true
                    self?.sendButton.alpha = 1
                } else {
                    self?.sendButton.isUserInteractionEnabled = false
                    self?.sendButton.alpha = 0.5
                }
                self?.sendButton.transform = CGAffineTransform.identity
            })
        }
    }
    
    fileprivate func setInputTextView() {
        inputTextView.delegate = self
        inputTextView.font = UIFont.systemFont(ofSize: 16)
        inputTextView.textAlignment = .left
        inputTextView.maxLength = 140
        inputTextView.placeholder = "Введите текст..."
        inputTextView.placeholderColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4, alpha: 1)
        inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        inputTextView.minHeight = 33.0
        inputTextView.maxHeight = 70.0
        inputTextView.trimWhiteSpaceWhenEndEditing = false
        inputTextView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        inputTextView.tintColor = .red
        inputTextView.layer.cornerRadius = inputTextView.bounds.height / 2
    }
    
    private func lockSendButtonAnimation() {
        if inputTextView.text?.cleanedText != nil || viewModel.capturedImages.count > 0 {
            sendButton.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
            sendButton.isHidden = true
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: { [weak self] in
                self?.sendButton.isHidden = false
                self?.sendButton.isUserInteractionEnabled = true
                self?.sendButton.alpha = 1
                self?.sendButton.transform = CGAffineTransform.identity
            })
        } else {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: { [weak self] in
                self?.sendButton.isUserInteractionEnabled = false
                self?.sendButton.alpha = 0.5
            })
        }
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: ChatAttachmentCell.typeName, bundle: nil) , forCellReuseIdentifier: ChatAttachmentCell.typeName)
        tableView.register(UINib(nibName: ChatImageViewCell.typeName, bundle: nil) , forCellReuseIdentifier: ChatImageViewCell.typeName)
        tableView.register(UINib(nibName: MessageUserCell.typeName, bundle: nil) , forCellReuseIdentifier: MessageUserCell.typeName)
        tableView.register(UINib(nibName: MessageSenderCell.typeName, bundle: nil) , forCellReuseIdentifier: MessageSenderCell.typeName)
        tableView.register(UINib(nibName: MessageReplySenderCell.typeName, bundle: nil) , forCellReuseIdentifier: MessageReplySenderCell.typeName)
        tableView.register(UINib(nibName: MessageReplyUserCell.typeName, bundle: nil) , forCellReuseIdentifier: MessageReplyUserCell.typeName)
        tableView.register(UINib(nibName: SenderNameCell.typeName, bundle: nil) , forCellReuseIdentifier: SenderNameCell.typeName)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
