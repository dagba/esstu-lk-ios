import Foundation
import RxSwift
import UtilityKit

/*TODO:
 - Rename Section to Section moddel
 - Inserrt in sectionList rows
 - Refactor mvvm to mvvm-c
 - Rebuild insert message logic
 - Set limit of fetched messages
 */

class ChatViewModel: BaseViewModel {
    enum SectionType {
        case read
        case sender
        case user
    }
    struct Section {
        var isMessageEmpty = false
        var type: SectionType = .read
        var countRow = 0
        var title = ""
    }
    private let disposeBag = DisposeBag()
    
    public var peerId = ""
    public var list = Variable([Message]())
    public var capturedImages = [String]()
    public var isMessageUploading = Variable(false)
    public var sectionList = [Section]()
    public var numberOfAddedSections = 0
    
    public var showReadCell: Bool {
        return list.value.first?.fromId == APIManager.shared.userId && list.value.first?.fromId != list.value.first?.peerId ? true : false
    }
    
    public var addedSectionIndex: Int {
        return showReadCell ? 1 : 0
    }
    
    override init() {
        super.init()
    }
    
    public func fetchMessageLog() {
        isLoading.value = true
        sectionList.removeAll()
        APIManager.shared.rx_fetchChatLog(peerId: peerId)
            .subscribe(onNext: { [weak self] (list) in
                guard let `self` = self else { return }
                
                self.list.value = list
                if self.showReadCell {
                    self.sectionList.append(Section(isMessageEmpty: true, type: .read, countRow: 1, title: ""))
                }
                for msg in list {
                    var section = Section()
                    section.isMessageEmpty = msg.text?.isEmpty ?? true
                    let sectionType: SectionType = msg.fromId == APIManager.shared.userId ? .user : .sender
                    section.type = sectionType
                    let attachmentsCount = msg.attachments?.count ?? 0
                    if section.isMessageEmpty {
                        section.countRow = sectionType == .user ? attachmentsCount : attachmentsCount + 1
                    } else {
                        section.countRow = attachmentsCount + 1
                    }
                    section.title =  msg.date?.string ?? "" //Helper.stringFromDateForMessage(date: msg.date)
                    self.sectionList.append(section)
                }
                self.isLoading.value = false
                self.markDialogAsRead()
            }, onError: { [weak self] (error) in
                self?.alertMessage.onNext(error)
                self?.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
    public var keyboardHeightObservable: Observable<CGFloat> {
        return Observable
            .from([
                NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
                    .map { notification -> CGFloat in
                        (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                },
                NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
                    .map { _ -> CGFloat in
                        0
                }
                ])
            .merge()
    }
    
    public func markDialogAsRead() {
        APIManager.shared.rx_markAsRead(peer: peerId, maxId: Int(list.value.first?.id ?? "") ?? 0)
    }
    
    public func sendMessage(with text: String?) {
//        insertMessage(with: text)
        isMessageUploading.value = true
        let text = text?.trimmingCharacters(in: .whitespaces).count != 0 ? text : nil
        APIManager.shared.rx_sendMessage(peerId: peerId, text: text, attachments: capturedImages)
            .subscribe(onNext: { [weak self] (id) in
                self?.capturedImages.removeAll()
                self?.isMessageUploading.value = false
                self?.fetchMessageLog()
                }, onError: { [weak self] error in
                    self?.alertMessage.onNext(error)
                    self?.isMessageUploading.value = false
//                    self?.removeLastMessage()
                    self?.isLoading.value = false
                    self?.capturedImages.removeAll()
            })
            .disposed(by: disposeBag)
    }
    
    public func uploadImage(image: UIImage, name: String) -> Observable<String> {
        return APIManager.shared.rx_uploadFile(image, name: name)
            .do(onNext: { [weak self] (code) in
                self?.capturedImages.append(code)
                }, onError: { [weak self] (error) in
                    self?.alertMessage.onNext(error)
            })
    }
    
    private func createMessage(text: String?, attachmentsUrl: [String]) -> Message {
        var msg = Message()
        msg.fromId = APIManager.shared.userId
        msg.peerId = peerId
        msg.text = text
        msg.date = Date()
        var attList = [Attachment]()
        for url in attachmentsUrl {
            var att = Attachment()
            att.fileId = url
            att.type = "image"
            att.name = url
            attList.append(att)
        }
        msg.attachments = attList
        
        return msg
    }
    
    private func insertMessage(with text: String?) {
        let message = self.createMessage(text: text, attachmentsUrl: self.capturedImages)
        let isMessageEmpty = text == nil ? true : false
        let messageCount = isMessageEmpty ? 0 : 1
        let title = message.date?.string ?? ""
        var rowCount = messageCount
        
        if let attachmentsCount = message.attachments?.count {
            rowCount += attachmentsCount
        }
        
        let sectionModel = Section(isMessageEmpty: isMessageEmpty, type: .user, countRow: rowCount, title: title)
        let readCellSectionModel = Section(isMessageEmpty: true, type: .read, countRow: 1, title: "")
        
        sectionList.insert(sectionModel, at: addedSectionIndex)
        if !showReadCell {
            numberOfAddedSections = 2
            sectionList.insert(readCellSectionModel, at: addedSectionIndex)
        }
        numberOfAddedSections = 1
        list.value.insert(message, at: addedSectionIndex)
    }
    
    private func removeLastMessage() {
        list.value.remove(at: 0)
        sectionList.remove(at: addedSectionIndex)
        if showReadCell {
            sectionList.remove(at: addedSectionIndex)
        }
    }
    
}
