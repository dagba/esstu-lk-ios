import UIKit
import NVActivityIndicatorView

class ChatAttachmentCell: ChatMessageTableViewCell {
    
    @IBOutlet private weak var fileImage: UIImageView!
    @IBOutlet private weak var fileNameLabel: UILabel!
    @IBOutlet private weak var fileSizeLabel: UILabel!
    @IBOutlet private weak var bgView: UIView!

    @IBOutlet private weak var trailingConstraint: NSLayoutConstraint! {
        didSet {
            cacheTrailingConstraint = trailingConstraint
        }
    }
    @IBOutlet private weak var leadingConstraint: NSLayoutConstraint! {
        didSet {
            cacheLeadingConstraint = leadingConstraint
        }
    }
    private weak var cacheLeadingConstraint: NSLayoutConstraint?
    private weak var cacheTrailingConstraint: NSLayoutConstraint?
    
    public var fileURL: URL?
    
    public func configure(attachment: Attachment, senderType: ChatViewModel.SectionType) {
        if senderType == .user {
            leadingConstraint?.isActive = false
        } else {
            trailingConstraint?.isActive = false
        }
        layoutIfNeeded()
        
        fileURL = URL(string: NetworkKey.baseDownloadingURL + (attachment.fileId ?? ""))
        fileNameLabel.text = attachment.name
        fileSizeLabel.text = attachment.size
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        trailingConstraint = cacheTrailingConstraint
        leadingConstraint = cacheLeadingConstraint
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fileImage.layer.cornerRadius = 8
    }
}
