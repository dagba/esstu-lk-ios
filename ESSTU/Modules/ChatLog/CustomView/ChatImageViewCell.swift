import UIKit
import Kingfisher

class ChatImageViewCell: ChatMessageTableViewCell {
    @IBOutlet private weak var mainImageView: UIImageView!
    public var showImage: ((UIImage?) -> ())?
    
    let kfOptions: [KingfisherOptionsInfoItem] = [
        .transition(.fade(1)),
        .cacheOriginalImage
    ]
    
    public func configure(attachment: Attachment, senderType: ChatViewModel.SectionType) {
        let urlString = NetworkKey.baseDownloadingURL + (attachment.fileId ?? "")
        let url = URL(string: urlString)!
        mainImageView.kf.setImage(
            with: url,
            placeholder: nil,
            options: kfOptions,
            progressBlock: nil
            )
        { [weak self] (result) in
            if result.value == nil { self?.mainImageView.backgroundColor = #colorLiteral(red: 0, green: 0.5254901961, blue: 0.5411764706, alpha: 1) }
            guard let `self` = self, let value = result.value else {
                return
            }
            
            let ratio = (value.image.width) / (value.image.height)
            
            let newWidth = (self.frame.height * ratio) > UIScreen.main.bounds.width ? (self.frame.height * ratio - 16.0) : (self.frame.height * ratio)
            DispatchQueue.main.async {
                self.mainImageView.frame.size = CGSize(width: newWidth, height: self.bounds.height)
                self.mainImageView.center = CGPoint(x: self.bounds.center.x, y:self.bounds.center.y)
                self.layoutIfNeeded()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let openTap = UITapGestureRecognizer(target: self, action: #selector(showImageShowVC(sender:)))
        openTap.numberOfTouchesRequired = 1
        openTap.numberOfTapsRequired = 1
        mainImageView.addGestureRecognizer(openTap)
        mainImageView.isUserInteractionEnabled = true
        mainImageView.kf.indicatorType = .activity
        mainImageView.contentMode = .scaleAspectFit
        mainImageView.layer.cornerRadius = 6
        mainImageView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @objc public func showImageShowVC(sender: UITapGestureRecognizer) {
        showImage?(mainImageView.image)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        mainImageView.image = nil
    }
}
