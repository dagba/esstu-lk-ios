import UIKit

class ImageShowViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - UI properties
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var closeImageView: UIImageView!
    private var imageView = UIImageView()
    private var mainImage: UIImage?
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalTransitionStyle = .crossDissolve
        setUpScrollView()
        setUpImageView()
        setGestureRecognizerToImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imageView.image = mainImage
        scrollView.addSubview(imageView)
        view.setNeedsDisplay()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imageView.frame.size = CGSize(width: scrollView.bounds.width, height: scrollView.bounds.height)
        view.layoutIfNeeded()
        scrollView.frame.size = CGSize(width: imageView.frame.width, height: imageView.frame.height)
    }
    
    // MARK: - Method delegate

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    // MARK: - Func definition
    
    private func setGestureRecognizerToImage() {
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(closeVC(sender:)))
        closeTap.numberOfTapsRequired = 1
        closeTap.numberOfTouchesRequired = 1
        closeImageView.addGestureRecognizer(closeTap)
    }
    
    private func setUpImageView() {
        closeImageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 6
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
    }
    
    private func setUpScrollView() {
        scrollView.delegate = self
        scrollView.maximumZoomScale = 5.0
        scrollView.minimumZoomScale = 1.0
        scrollView.layer.cornerRadius = 6
        scrollView.layer.masksToBounds = true
    }

    public func setImage(_ image: UIImage?) {
        mainImage = image
    }
    
    @objc public func closeVC(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }

}
