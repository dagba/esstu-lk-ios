import UIKit

class MessageReplySenderCell: ChatMessageTableViewCell {

    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var replyTextLabel: UILabel!
    @IBOutlet private weak var replyNameLabel: UILabel!
    @IBOutlet private  weak var senderNameLabel: UILabel!
    private var tapGesture: UITapGestureRecognizer? {
        didSet {
            bgView.addGestureRecognizer(tapGesture!)
        }
    }
    
    private var closure: () -> () = { print(1) }
    @objc private func triggerClosure() {
        closure()
    }
    
    public func configure(msg: Message, replyMsg: Message, tapAction: @escaping () -> ()) {
        var senderDisplayName: String {
            let first = "\(msg.fromUser?.lastName ?? "") \(msg.fromUser?.firstName ?? "")"
            let second = "\(msg.fromUser?.firstName ?? "") \(msg.fromUser?.middleName ?? "")"
            return msg.fromUser?.userId?.first == "e" ? second : first
        }
        var replySenderDisplayName: String {
            let first = "\(replyMsg.fromUser?.lastName ?? "") \(replyMsg.fromUser?.firstName ?? "")"
            let second = "\(replyMsg.fromUser?.firstName ?? "") \(replyMsg.fromUser?.middleName ?? "")"
            return replyMsg.fromUser?.userId?.first == "e" ? second : first
        }
        
        textView.text = msg.text
        replyNameLabel.text = replySenderDisplayName
        replyTextLabel.text = replyMsg.text
        senderNameLabel.text = senderDisplayName
        
        closure = tapAction
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(triggerClosure))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 8
        bgView.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textView.text?.removeAll()
        replyNameLabel.text?.removeAll()
        replyTextLabel.text?.removeAll()
    }
    
}
