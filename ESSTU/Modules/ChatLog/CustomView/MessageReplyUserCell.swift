import UIKit

class MessageReplyUserCell: ChatMessageTableViewCell {
    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var replyTextLabel: UILabel!
    @IBOutlet private weak var replyNameLabel: UILabel!
    private var tapGesture: UITapGestureRecognizer? {
        didSet {
            bgView.addGestureRecognizer(tapGesture!)
        }
    }
    
    private var closure: () -> () = { print(1) }
    @objc func triggerClosure() {
        closure()
    }
    
    public func configure(msg: Message, replyMsg: Message, tapAction: @escaping () -> ()) {
        var replySenderName: String {
            let first = "\(replyMsg.fromUser?.lastName ?? "") \(replyMsg.fromUser?.firstName ?? "")"
            let second = "\(replyMsg.fromUser?.firstName ?? "") \(replyMsg.fromUser?.middleName ?? "")"
            return replyMsg.fromUser?.userId?.first == "e" ? second : first
        }
        
        textView.text = msg.text
        replyNameLabel.text = replySenderName
        replyTextLabel.text = replyMsg.text
        
        closure = tapAction
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(triggerClosure))
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 8
        bgView.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textView.text?.removeAll()
        replyNameLabel.text?.removeAll()
        replyTextLabel.text?.removeAll()
    }
    
}
