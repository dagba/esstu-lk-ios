import UIKit

class MessageSenderCell: ChatMessageTableViewCell {

    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var senderNameLabel: UILabel!
    
    public func configure(msg: Message) {
        var senderDisplayName: String {
            let first = "\(msg.fromUser?.lastName ?? "") \(msg.fromUser?.firstName ?? "")"
            let second = "\(msg.fromUser?.firstName ?? "") \(msg.fromUser?.middleName ?? "")"
            return msg.fromUser?.userId?.first == "e" ? second : first
        }
        
        senderNameLabel.text = senderDisplayName
        textView.text = msg.text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textView.layer.cornerRadius = 8
        textView.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textView.text?.removeAll()
    }
    
    
}
