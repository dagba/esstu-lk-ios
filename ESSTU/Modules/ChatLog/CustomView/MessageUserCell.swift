import UIKit

class MessageUserCell: ChatMessageTableViewCell {
    @IBOutlet private weak var textView: UITextView!
    
    public func configure(msg: Message) {
        textView.text = msg.text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textView.layer.cornerRadius = 8
        textView.layer.masksToBounds = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textView.text = ""
    }
    
}
