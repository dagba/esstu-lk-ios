import UIKit

class ReadCell: ChatMessageTableViewCell {
    
    @IBOutlet weak var readLabel: UILabel!

    public func configure(msg: Message?) {
        guard let msg = msg else { return }
        let count = msg.countViews ?? 0
        readLabel.text = count > 1 ? "Прочитано" : "Не прочитано"
    }

}
