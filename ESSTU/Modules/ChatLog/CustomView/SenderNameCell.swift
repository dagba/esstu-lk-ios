import UIKit

class SenderNameCell: ChatMessageTableViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!

    public func configure(with text: String?) {
        nameLabel?.text = text
    }
    
}
