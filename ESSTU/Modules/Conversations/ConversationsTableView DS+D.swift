import UIKit
import RxSwift
import RxCocoa

extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != viewModel.sortedArray.value.count else { return }
        searchBar.resignFirstResponder()
        let conversation = viewModel.sortedArray.value[indexPath.row]
        let chatVC = Assembler.createChatViewController() as! ChatViewController

        chatVC.navigationItem.title = conversation.chatDescription?.title
        chatVC.viewModel.peerId = conversation.peerId ?? ""
        navigationController?.pushViewController(chatVC, animated: true)
        conversation.unreadCount = 0
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == viewModel.sortedArray.value.count  && !viewModel.isPaginationCellDeleted {
            let cell = tableView.dequeueReusableCell(withIdentifier: PaginationTableViewCell.typeName, for: indexPath) as! PaginationTableViewCell
            cell.closure = { [unowned self] in
                self.viewModel.offset.value += NetworkKey.offset
            }
            cell.button.isHidden = false
            cell.spinnerView.isHidden = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationTableViewCell.typeName, for: indexPath) as! ConversationTableViewCell
        cell.conversation = viewModel.sortedArray.value[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "\u{21e4}\n Выйти") { (action, indexPath) in
            let description = "Вы действительно желаете покинуть \"\(self.viewModel.array.value[indexPath.row].chatDescription?.title ?? "")\"?"
            let alert = UIAlertController(title: "Выйти", message: description, preferredStyle: UIAlertController.Style.actionSheet)
            let no = UIAlertAction(title: "Отмена", style: .cancel) { _ in
                alert.dismiss(animated: true, completion: nil)
            }
            let yes = UIAlertAction(title: "Да", style: UIAlertAction.Style.destructive) { [weak self] _ in
                guard self?.viewModel.sortedArray.value.count == self?.viewModel.array.value.count else {
                    self?.showAlert(with: "Выход прошел неудачно\n Попробуйте обновить список")
                    return
                }
                self?.viewModel.quitFromDialog(peerId: self?.viewModel.array.value[indexPath.row].peerId ?? "", row: indexPath.row)
            }
            alert.addAction(no)
            alert.addAction(yes)
            self.present(alert, animated: true, completion: nil)
        }
        
        return [delete]
    }
    
    /// scrolls to top when tap on tabBar
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let index = tabBarController.selectedIndex
        
        scrollToTop(index, tabBarController)
        
        return true
    }
    
}
