import Foundation
import RxSwift

class ConversationsViewModel: BaseViewModel {
    let disposeBag = DisposeBag()
    let array      = Variable([Dialog]())
    let sortedArray = Variable([Dialog]())
    let offset     = Variable(0)
    let isUserQuited = Variable(false)
    var quitedChatRow = 0
    var count: Int {
        if isPaginationCellDeleted {
            return sortedArray.value.count
        } else {
            return sortedArray.value.count == 0 ? sortedArray.value.count : sortedArray.value.count + 1
        }
    }
    var isPaginationCellDeleted = false
    
    override init() {
        super.init()
        
        array.asObservable()
            .bind(to: sortedArray)
            .disposed(by: disposeBag)
    }
    
    func fetchConversations() {
        isPaginationCellDeleted = false
        offset.value = 0
        isLoading.value = true
        sortedArray.value.removeAll()
        APIManager.shared.rx_fetchConversations()
            .subscribe(onNext: { [weak self] (conversations) in
                if (conversations.count % NetworkKey.offset) != 0 {
                    self?.isPaginationCellDeleted  = true
                } else {
                    self?.isPaginationCellDeleted  = false
                }
                self?.array.value = conversations
                self?.isLoading.value = false
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
    func filterList(with text: String) {
        isLoading.value = true
        sortedArray.value.removeAll()
        let searchedText = text.lowercased()
        guard searchedText != "" else {
            if (array.value.count % NetworkKey.offset) != 0 {
                isPaginationCellDeleted  = true
            } else {
                isPaginationCellDeleted  = false
            }
            sortedArray.value = array.value
            isLoading.value = false /// reloads table view
            return
        }
        APIManager.shared.rx_fetchConversations(with: 0, limit: 999999)
            .subscribe(onNext: { [weak self] (conversations) in
                self?.isPaginationCellDeleted = true
                self?.sortedArray.value = conversations.filter { (dialog) -> Bool in
                    return dialog.chatDescription?.title?.lowercased().range(of: searchedText) != nil ||
                        dialog.preview?.text?.lowercased().range(of: searchedText) != nil
                }
                self?.isLoading.value = false
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.sortedArray.value = self.array.value
                    self.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
    func fetchConversationsWithOffset() -> Observable<[Dialog]> {
        return APIManager.shared.rx_fetchConversations(with: offset.value)
    }
    
    func quitFromDialog(peerId: String, row: Int) {
        guard peerId != "" else { return }
        quitedChatRow = row
        APIManager.shared.rx_deleteUserFromDialog(peerId: peerId)
            .subscribe(onNext: { [weak self] (isQuited) in
                self?.isUserQuited.value = isQuited
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.isUserQuited.value = false
            })
            .disposed(by: disposeBag)
    }
    
}
