import UIKit

class ConversationTableViewCell: BaseDialogTableViewCell {
    
    @IBOutlet weak var unreadCountLabel : UILabel!
    @IBOutlet weak var titleLabel        : UILabel!
    @IBOutlet weak var dateLabel         : UILabel!
    @IBOutlet weak var bodyLabel         : UILabel!
    @IBOutlet weak var senderNameLabel   : UILabel!
    
    var conversation: Dialog? {
        didSet {
            if let conversation = conversation {
                titleLabel.text = conversation.chatDescription?.title
                dateLabel.text = conversation.preview?.date?.string
                let sender = conversation.user
                senderNameLabel.text = "\(sender?.lastName ?? "") \(sender?.firstName?.first ?? " ".first!).\(sender?.middleName?.first ?? " ".first!). "
                bodyLabel.text = conversation.preview?.text
                if let unreadCount = conversation.unreadCount, unreadCount > 0 {
                    unreadCountLabel.layer.cornerRadius = unreadCountLabel.bounds.width / 2
                    unreadCountLabel.layer.masksToBounds = true
                    unreadCountLabel.isHidden = false
                    unreadCountLabel.text = String(unreadCount)
                } else {
                    unreadCountLabel.isHidden = true
                }
                
            }
        }
    }
    
}
