import UIKit
import Kingfisher
import InitialsImageView
import UtilityKit

class DialogueTableViewCell: BaseDialogTableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var dateLabel        : UILabel!
    @IBOutlet weak var bodyLabel        : UILabel!
    @IBOutlet weak var senderNameLabel  : UILabel!
    @IBOutlet weak var unreadCountLabel : UILabel!
    @IBOutlet weak var profileImageView : UIImageView!
    
    var fullName: String {
        guard let dialog = dialog else { return "" }
        return APIManager.shared.userId == dialog.preview?.fromId ? "\(dialog.preview?.peerUser?.lastName ?? "") \(dialog.preview?.peerUser?.firstName ?? "") \(dialog.preview?.peerUser?.middleName ?? "")" : "\(dialog.preview?.fromUser?.lastName ?? "") \(dialog.preview?.fromUser?.firstName ?? "") \(dialog.preview?.fromUser?.middleName ?? "")"
    }
    
    var imageName: String {
        guard let dialog = dialog else { return "" }
        return APIManager.shared.userId == dialog.preview?.fromId ? "\(dialog.preview?.peerUser?.lastName ?? "") \(dialog.preview?.peerUser?.firstName ?? "")" : "\(dialog.preview?.fromUser?.lastName ?? "") \(dialog.preview?.fromUser?.firstName ?? "")"
    }
    
    var photoId: String? {
        guard let dialog = dialog else { return "" }
        return APIManager.shared.userId == dialog.preview?.fromId ? dialog.preview?.peerUser?.photo : dialog.preview?.fromUser?.photo
    }
    
    var dialog: Dialog? {
        didSet {
            if let dialog = dialog {
                nameLabel.text = fullName
                if APIManager.shared.userId == dialog.preview?.fromUser?.userId {
                    senderNameLabel.text = "Вы: "
                } else {
                    senderNameLabel.text = ""
                }
                if let photoId = self.photoId {
                    profileImageView.kf.setImage(with: URL(string: NetworkKey.baseDownloadingURL + photoId), placeholder: nil, options: nil)
                    { [weak self] result in
                        guard let `self` = self else { return }
                        
                        DispatchQueue.main.async {
                            self.profileImageView.round()
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.profileImageView.setImageForName(self.imageName, circular: true, textAttributes: nil)
                        self.layoutIfNeeded()
                    }
                }
                dateLabel.text = dialog.preview?.date?.string
                dateLabel.sizeToFit()
                bodyLabel.text = dialog.preview?.text
                descriptionLabel.text = APIManager.shared.userId == dialog.preview?.fromId ? dialog.preview?.peerUser?.description : dialog.preview?.fromUser?.description
                if let unreadCount = dialog.unreadCount, unreadCount > 0 {
                    unreadCountLabel.layer.cornerRadius = unreadCountLabel.bounds.width / 2
                    unreadCountLabel.layer.masksToBounds = true
                    unreadCountLabel.isHidden = false
                    unreadCountLabel.text = String(unreadCount)
                } else {
                    unreadCountLabel.isHidden = true
                }
            }
            
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        profileImageView.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.kf.indicatorType = .none
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
    }
    
}
