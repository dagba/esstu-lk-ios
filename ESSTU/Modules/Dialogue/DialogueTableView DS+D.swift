import UIKit
import RxSwift
import RxCocoa

extension DialogueViewController: UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != viewModel.sortedArray.value.count else { return }
        
        searchBar.resignFirstResponder()
        let chatVC = Assembler.createChatViewController() as! ChatViewController
        let dialog = viewModel.sortedArray.value[indexPath.row]
        let senderName = APIManager.shared.userId == dialog.preview?.fromId ? "\(dialog.preview?.peerUser?.lastName ?? "") \(dialog.preview?.peerUser?.firstName ?? "")" : "\(dialog.preview?.fromUser?.lastName ?? "") \(dialog.preview?.fromUser?.firstName ?? "")"
        
        chatVC.navigationItem.title = senderName
        chatVC.viewModel.peerId = dialog.peerId ?? ""
        let cell = tableView.cellForRow(at: indexPath) as! DialogueTableViewCell
        chatVC.photoImage = cell.profileImageView.image
        navigationController?.pushViewController(chatVC, animated: true)
        dialog.unreadCount = 0
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == viewModel.sortedArray.value.count  && !viewModel.isPaginationCellDeleted {
            let cell = tableView.dequeueReusableCell(withIdentifier: PaginationTableViewCell.typeName, for: indexPath) as! PaginationTableViewCell
            cell.closure = { [unowned self] in
                self.viewModel.offset.value += NetworkKey.offset
            }
            cell.button.isHidden = false
            cell.spinnerView.isHidden = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DialogueTableViewCell.typeName, for: indexPath) as! DialogueTableViewCell
        cell.dialog = viewModel.sortedArray.value[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let defaultHeight = UIScreen.main.bounds.height / 8
        let height = defaultHeight > 90 ? 90 : defaultHeight
        return height
    }
    
    /// scrolls to top when tap on tabBar
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if let vc = viewController as? UINavigationController, vc.viewControllers.last == self {
//            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
//        }
    
        let index = tabBarController.selectedIndex
        
        scrollToTop(index, tabBarController)
        
        return true
    }
    
}
