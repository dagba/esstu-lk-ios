import UIKit
import RxCocoa
import RxSwift
import Material

class DialogueViewController: ViewController {
    var viewModel  = DialogueViewModel()
    private let disposeBag = DisposeBag()
    
    /// MARK: - Properties and UI connections
    private let refresh = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            setUpTableView()
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
        viewModel.fetchDialogs()
        setRefreshControl()
        setNavigationButtons()
    }
    
}

/// MARK: - Func definition

extension DialogueViewController {
    
    private func bindViewModel() {
        
        viewModel.alertMessage
            .bind { [weak self] (error) in
                self?.showAlert(with: error)
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .bind(onNext: { loading in
                if !loading {
                    self.tableView.reloadData()
                }
                loading ? self.view.makeToastActivity(.center) : self.view.hideToastActivity()
            })
            .disposed(by: disposeBag)
        
        refresh.rx.controlEvent(.valueChanged)
            .filter { self.refresh.isRefreshing  }
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.fetchDialogs()
                self?.refresh.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        viewModel.sortedArray.asObservable()
            .filter { $0.count == 0 }
            .bind { [weak self] _ in
                self?.tableView.reloadData()
            }
            .disposed(by: disposeBag)
        
        viewModel.offset.asObservable()
            .filter { $0 != 0 }
            .flatMap { _ -> Observable<[Dialog]> in
                return self.viewModel.fetchDialogesWithOffset()
            }
            .subscribe(onNext: { [unowned self] (dialogs) in
                var indexPath = [IndexPath]()
                var row = self.viewModel.sortedArray.value.count
                for _ in dialogs {
                    indexPath.append(IndexPath(row: row, section: 0))
                    row += 1
                }
                if dialogs.count < NetworkKey.offset {
                    self.viewModel.isPaginationCellDeleted = true
                    let pagiIndexPath = IndexPath(row: self.viewModel.sortedArray.value.count, section: 0)
                    self.tableView.deleteRows(at: [pagiIndexPath], with: .none)
                } else {
                    self.viewModel.isPaginationCellDeleted = false
                }
                self.viewModel.array.value.append(contentsOf: dialogs)
                self.tableView.insertRows(at: indexPath, with: .top)
                }, onError: { [weak self] (error) in
                    self?.showAlert(with: error)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.textDidBeginEditing
            .subscribe { [weak self] _ in
                self?.searchBar.setShowsCancelButton(true, animated: true)
            }
            .disposed(by: disposeBag)
        
        searchBar.rx.text
            .skip(1)
            .distinctUntilChanged()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (searchedText) in
                self?.viewModel.filterList(with: searchedText ?? "")
            })
            .disposed(by: disposeBag)
        
        Observable.of(searchBar.rx.searchButtonClicked, searchBar.rx.cancelButtonClicked).merge()
            .subscribe { [weak self] _ in
                self?.searchBar.setShowsCancelButton(false, animated: true)
                self?.searchBar.resignFirstResponder()
            }
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .subscribe(onNext: { [weak self] _ in
                self?.searchBar.text?.removeAll()
                self?.viewModel.filterList(with: "")
            })
            .disposed(by: disposeBag)
        
    }
    
    private func setUpTableView() {
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tabBarController?.delegate = self
        tableView.register(UINib(nibName: PaginationTableViewCell.typeName, bundle: nil) , forCellReuseIdentifier: PaginationTableViewCell.typeName)
    }
    
    private func setRefreshControl() {
        refresh.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        tableView.addSubview(refresh)
    }
    
    private func setNavigationButtons() {
        navigationController?.navigationBar.largeTitleTextAttributes = nil

//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Add"), style: .plain, target: self, action: #selector(createNewDialog))
//        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 0.244529566, blue: 0.2133625337, alpha: 1)
//        navigationItem.rightBarButtonItem?.width = 7
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.red,
                                                                   NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .headline)]
    }
    
    @objc private func createNewDialog() {
        
    }
    
}
