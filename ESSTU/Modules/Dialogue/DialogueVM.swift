import Foundation
import RxSwift

class DialogueViewModel: BaseViewModel {
    let disposeBag  = DisposeBag()
    let array       = Variable([Dialog]())
    let sortedArray = Variable([Dialog]())
    let offset      = Variable(0)
    var count: Int {
        if isPaginationCellDeleted {
            return sortedArray.value.count
        } else {
            return sortedArray.value.count == 0 ? sortedArray.value.count : sortedArray.value.count + 1
        }
    }
    var isPaginationCellDeleted = false
    
    override init() {
        super.init()
        
        array.asObservable()
            .bind(to: sortedArray)
            .disposed(by: disposeBag)
    }
    
    func fetchDialogs() {
        isPaginationCellDeleted = false
        offset.value = 0
        isLoading.value = true
        sortedArray.value.removeAll()
        APIManager.shared.rx_fetchDialogs()
            .subscribe(onNext: { [weak self] (dialogs) in
                if (dialogs.count % NetworkKey.offset) != 0 {
                    self?.isPaginationCellDeleted  = true
                } else {
                    self?.isPaginationCellDeleted  = false
                }
                self?.array.value = dialogs
                self?.isLoading.value = false
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
    func fetchDialogesWithOffset() -> Observable<[Dialog]> {
        return APIManager.shared.rx_fetchDialogs(with: offset.value)
    }
    
    func filterList(with text: String) {
        isLoading.value = true
        sortedArray.value.removeAll()
        let searchedText = text.lowercased()
        guard searchedText != "" else {
            if (array.value.count % NetworkKey.offset) != 0 {
                isPaginationCellDeleted  = true
            } else {
                isPaginationCellDeleted  = false
            }
            sortedArray.value = array.value
            isLoading.value = false /// reloads table view
            return
        }
        
        APIManager.shared.rx_fetchDialogs(with: 0, limit: 9999999)
            .subscribe(onNext: { [weak self] (dialogs) in
                self?.isPaginationCellDeleted = true
                self?.sortedArray.value = dialogs.filter({ (dialog) -> Bool in
                    let senderName = APIManager.shared.userId == dialog.preview?.fromId ? "\(dialog.preview?.peerUser?.lastName ?? "") \(dialog.preview?.peerUser?.firstName ?? "") \(dialog.preview?.peerUser?.middleName ?? "")" : "\(dialog.preview?.fromUser?.lastName ?? "") \(dialog.preview?.fromUser?.firstName ?? "") \(dialog.preview?.fromUser?.middleName ?? "")"
                    return senderName.lowercased().range(of: searchedText) != nil ||
                        dialog.preview?.text?.lowercased().range(of: searchedText) != nil
                })
                self?.isLoading.value = false
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.sortedArray.value = self.array.value
                    self.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
}
