import UIKit
import UtilityKit

class MainTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //// Remove top separator
        tabBar.makeClear()
        tabBar.isTranslucent = false
        tabBar.backgroundColor = .white
        tabBar.tintColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4, alpha: 1)
        //// Set activity loading style
        ToastManager.shared.style.activitySize = CGSize(width: 77, height: 77)
        ToastManager.shared.style.activityIndicatorColor = .yellow
        ToastManager.shared.style.activityBackgroundColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4, alpha: 1).withAlphaComponent(0.7)
    }

}
