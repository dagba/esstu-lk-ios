import UIKit
import RxSwift
import Firebase
import RxCocoa
import UtilityKit
import Material

class MenuViewController: UITableViewController {
    let disposeBag = DisposeBag()
    
    /// MARK: - Properties and UI connections
    var user: User? {
        didSet {
            if let user = user {
                userNameLabel.text = "\(user.lastName ?? "") \(user.firstName ?? "") \(user.middleName ?? "")"
                userDescriptionLabel.text = user.description
                if let photoId = user.photo {
                    profileImageView.kf.setImage(with: URL(string: NetworkKey.baseDownloadingURL + photoId), placeholder: nil, options: nil)
                    { [weak self] result in
                        guard let `self` = self else { return }
                        
                        DispatchQueue.main.async {
                            self.profileImageView.contentMode = .scaleAspectFill
                            self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.width / 2
                            self.profileImageView.layer.masksToBounds = true
                            self.view.layoutIfNeeded()
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.profileImageView.setImageForName("\(user.lastName ?? "") \(user.firstName ?? "")", circular: true, textAttributes: nil)
                        self.profileImageView.setDeepShadow()
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var profileTableViewCell: UITableViewCell!
    @IBOutlet weak var userDescriptionLabel : UILabel!
    @IBOutlet weak var userNameLabel        : UILabel!
    @IBOutlet weak var profileImageView     : UIImageView!
    @IBOutlet weak var switcher             : UISwitch!
    
    /// MARK: - UI Actions
    
    @IBAction func logOut(sender: UIButton) {
        self.view.makeToastActivity(.center)

        InstanceID.instanceID().deleteID { [weak self] error in
            if let error = error {
                self?.view.hideToastActivity()
                self?.showAlert(title: nil, message: error.localizedDescription, completion: nil)
            } else {
                self?.view.hideToastActivity()
                APIManager.shared.logout()
                self?.present(UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()!, animated: true)
            }
        }
    }
    
    /// MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        navigationController?.navigationBar.tintColor = .red
        switcher.isOn = UserDefaults.standard.bool(forKey: "SCHEDULE_UPDATE")
        profileImageView.image = nil
        APIManager.shared.rx_fetchUser(id: APIManager.shared.userId)
            .subscribe(onNext: { [weak self] (user) in
                self?.user = user
            }, onError: { (error) in
                print(error)
            })
            .disposed(by: disposeBag)
        setUpProfileCell()
        setupNavBar()
        switcher.rx.controlEvent(UIControlEvents.valueChanged)
            .asObservable()
            .subscribe(onNext: { [unowned self] _ in
                if self.switcher.isOn {
                    UserDefaults.standard.set(true, forKey: "SCHEDULE_UPDATE")
                } else {
                    UserDefaults.standard.set(false, forKey: "SCHEDULE_UPDATE")
                }
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.tintColor = .red
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.allowsSelection = true
    }
    
}

/// MARK: - Datasource+Delegate methods

extension MenuViewController {
    
    private func setupTableView() {
        tableView.removeSeparatorsForEmptyCells()
        tableView.allowsSelection = true
    }
    
    private func setUpProfileCell() {
        userNameLabel.font = Material.RobotoFont.medium(with: 16)
        userNameLabel.text                   = APIManager.shared.userFullName
        userDescriptionLabel.text            = APIManager.shared.userInfo
        profileImageView.layer.cornerRadius  = profileImageView.bounds.width / 2
        profileImageView.layer.masksToBounds = false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            print("12312jhk")
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
}
