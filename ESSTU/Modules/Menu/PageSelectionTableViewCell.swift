//
//  PageSelectionTableViewCell.swift
//  ESSTU
//
//  Created by d on 14/05/2019.
//  Copyright © 2019 dagba. All rights reserved.
//

import UIKit

class PageSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet private var titleLabel: UILabel!
    
    public var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        accessoryType = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        accessoryType = .none
    }

}
