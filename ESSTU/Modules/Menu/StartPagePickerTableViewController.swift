//
//  StartPagePickerTableViewController.swift
//  ESSTU
//
//  Created by d on 14/05/2019.
//  Copyright © 2019 dagba. All rights reserved.
//

import UIKit
import UtilityKit

class StartPagePickerTableViewController: UITableViewController {
    
    private var pageStates = [false, false, false, false]
    private let pageTitles = ["Расписание", "Объявления", "Диалоги", "Обсуждения"]
    private var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        let index = UserDefaults.standard.integer(forKey: "START_PAGE_INDEX")
        
        navigationItem.title = "Выбор стартового экрана"
        navigationController?.navigationBar.tintColor = .red
        selectedIndex = index
        pageStates[index] = true
        clearsSelectionOnViewWillAppear = true
        tableView.removeSeparatorsForEmptyCells()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pageStates.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PageSelectionTableViewCell.typeName, for: indexPath) as! PageSelectionTableViewCell

        cell.title = pageTitles[indexPath.row]
        cell.accessoryType = pageStates[indexPath.row] ? .checkmark : .none

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0))?.accessoryType = .none
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        tableView.deselectRow(at: indexPath, animated: true)
        selectedIndex = indexPath.row
        UserDefaults.standard.set(indexPath.row, forKey: "START_PAGE_INDEX")
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
