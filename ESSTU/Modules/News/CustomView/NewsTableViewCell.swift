import UIKit
import Material
import UtilityKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var attachmentsCountLabel: UILabel!
    @IBOutlet weak var mainBackgroundView    : UIView!
    @IBOutlet weak var titleLabel            : UILabel!
    @IBOutlet weak var nameLabel             : UILabel!
    @IBOutlet weak var dateLabel             : UILabel!
    @IBOutlet weak var descriptionLabel      : UILabel!
    @IBOutlet weak var messageTextLabel      : UITextView!
    @IBOutlet weak var countViewsLabel       : UILabel!
    @IBOutlet weak var membersLabel          : UILabel!
    @IBOutlet weak var attachmentCountButton : UIButton!
    
    public var dialog: Dialog? {
        didSet {
            if let dialog = dialog {
                titleLabel.text = dialog.chatDescription?.title
                nameLabel.text = "\(dialog.user?.lastName ?? "Unknown") \(dialog.user?.firstName ?? "Unknown") \(dialog.user?.middleName ?? "Unknown")"
                dateLabel.text = dialog.preview?.date?.string
                descriptionLabel.text = dialog.user?.description
                messageTextLabel.text = dialog.preview?.text
                countViewsLabel.text = String(dialog.preview?.countViews ?? 0)
                membersLabel.text = String(dialog.chatDescription?.participantsCount ?? 0)
                if let attachments = dialog.preview?.attachments, attachments.count >= 1 {
                    attachmentCountButton.isHidden = false
                    attachmentsCountLabel.isHidden = false
                    attachmentsCountLabel.text = String(attachments.count)
                    
                } else {
                    attachmentsCountLabel.isHidden = true
                    attachmentCountButton.isHidden = true
                }
            }
        }
    }
    
    @IBAction func participantsButtonTapped(_ sender: UIButton) {
        let vc = Assembler.createUsersListViewController() as! UserListViewController
        vc.peerId = dialog?.peerId
        vc.title = dialog?.chatDescription?.title
        UIViewController.getTopViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainBackgroundView.layer.borderWidth = 1
        mainBackgroundView.layer.borderColor = UIColor.clear.cgColor
        mainBackgroundView.layer.cornerRadius = 4
        mainBackgroundView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
        mainBackgroundView.layer.shadowOpacity = 0.3
        mainBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        mainBackgroundView.layer.shadowRadius = 2
        attachmentCountButton.imageView?.contentMode = .scaleAspectFit
        titleLabel.font = Material.RobotoFont.medium(with: 19)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowPath = UIBezierPath(roundedRect: mainBackgroundView.bounds,
                                        byRoundingCorners: .allCorners,
                                        cornerRadii: CGSize(width: 3, height: 3)).cgPath
    }
    
}
