import UIKit
import RxSwift
import RxCocoa

extension NewsViewController: UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == viewModel.sortedArray.value.count && !viewModel.isPaginationCellDeleted {
            let cell = tableView.dequeueReusableCell(withIdentifier: PaginationTableViewCell.typeName, for: indexPath) as! PaginationTableViewCell
            cell.closure = { [unowned self] in
                self.viewModel.offset.value += NetworkKey.offset
            }
            cell.button.isHidden = false
            cell.spinnerView.isHidden = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.typeName, for: indexPath) as! NewsTableViewCell
        cell.dialog = viewModel.sortedArray.value[indexPath.row]
        
        /// set a number of row to identify a cell when seguing to AttachmentsViewController. See in NewsVM
        cell.attachmentCountButton.tag = indexPath.row

        return cell
    }
    
    //// scrolls to top when tap on tabBar
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let index = tabBarController.selectedIndex

        scrollToTop(index, tabBarController)
        
        return true
    }

}

func scrollToTop(_ index: Int, _ tabBarController: UITabBarController) {
    switch index {
    case 1:
        if let navVC = tabBarController.viewControllers?[1] as? UINavigationController,
            let vc = navVC.viewControllers.last as? NewsViewController,
            vc.viewIfLoaded?.window != nil {
            
            vc.tableView?.setContentOffset(CGPoint.zero, animated: true)
        }
    case 2:
        if let navVC = tabBarController.viewControllers?[2] as? UINavigationController,
            let vc = navVC.viewControllers.last as? DialogueViewController,
            vc.viewIfLoaded?.window != nil  {
            
            vc.tableView?.setContentOffset(CGPoint.zero, animated: true)
        }
    case 3:
        if let navVC = tabBarController.viewControllers?[3] as? UINavigationController,
            let vc = navVC.viewControllers.last as? ConversationsViewController,
            vc.viewIfLoaded?.window != nil  {
            
            vc.tableView?.setContentOffset(CGPoint.zero, animated: true)
        }
    default:
        break
    }
}
