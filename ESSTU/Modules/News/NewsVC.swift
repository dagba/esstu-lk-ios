import UIKit
import RxCocoa
import RxSwift
import Material

class NewsViewController: ViewController {
    private let disposeBag = DisposeBag()
    public  var viewModel  = NewsViewModel()
    
    /// MARK: - Properties and UI connections
    private var animationDirection = AnimateDirection.down
    private let refresh = UIRefreshControl()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            setUpTableView()
        }
    }
    
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setRefreshControl()
        setNavigationButtons()
        bindViewModel()
        viewModel.fetchAnnouncements()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      viewModel.doTransitionWith(segue, sender: sender)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        viewModel.searchDialogs = nil
    }
    
}

/// MARK: - Func definition

extension NewsViewController {
    
    private func bindViewModel() {
        
        viewModel.alertMessage
            .bind { [weak self] (error) in
                self?.showAlert(with: error)
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .bind(onNext: { loading in
                if !loading {
                    AnimatableReload.reload(tableView: self.tableView, animationDirection: self.animationDirection)
                }
                loading ? self.view.makeToastActivity(.center) : self.view.hideToastActivity()
            })
            .disposed(by: disposeBag)
        
        refresh.rx.controlEvent(.valueChanged)
            .filter { self.refresh.isRefreshing  }
            .subscribe(onNext: { [weak self] _ in
                self?.animationDirection = .down
                self?.viewModel.fetchAnnouncements()
                self?.refresh.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        viewModel.sortedArray.asObservable()
            .filter { $0.count == 0 }
            .bind { _ in AnimatableReload.reload(tableView: self.tableView, animationDirection: .none) }
            .disposed(by: disposeBag)
        
        viewModel.offset.asObservable()
            .filter { $0 != 0 }
            .flatMap { _ -> Observable<[Dialog]> in
                return self.viewModel.fetchAnnouncementsWithOffset()
            }
            .subscribe(onNext: { [unowned self] (announcements) in
                var indexPath = [IndexPath]()
                var row = self.viewModel.array.value.count
                for _ in announcements {
                    indexPath.append(IndexPath(row: row, section: 0))
                    row += 1
                }
                if announcements.count < NetworkKey.offset {
                    self.viewModel.isPaginationCellDeleted = true
                    let pagiIndexPath = IndexPath(row: self.viewModel.array.value.count, section: 0)
                    self.tableView.deleteRows(at: [pagiIndexPath], with: .none)
                } else {
                    self.viewModel.isPaginationCellDeleted = false
                }
                self.viewModel.array.value.append(contentsOf: announcements)
                self.tableView.insertRows(at: indexPath, with: .top)
            }, onError: { [weak self] (error) in
                self?.showAlert(with: error)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.textDidBeginEditing
            .subscribe { [weak self] _ in
                self?.animationDirection = .none
                self?.searchBar.setShowsCancelButton(true, animated: true)
            }
            .disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
//            .skip(1)
            .throttle(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                let searchText = self?.searchBar.text
                self?.viewModel.filterList(with: searchText ?? "")
            })
            .disposed(by: disposeBag)
        
        Observable.of(searchBar.rx.searchButtonClicked, searchBar.rx.cancelButtonClicked).merge()
            .subscribe { [weak self] _ in
                self?.searchBar.setShowsCancelButton(false, animated: true)
                self?.searchBar.resignFirstResponder()
            }
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .subscribe(onNext: { [weak self]_ in
                self?.searchBar.text?.removeAll()
                self?.viewModel.filterList(with: "")
            })
            .disposed(by: disposeBag)
        
        viewModel.badgeValue.asObservable()
            .skip(1)
            .distinctUntilChanged()
            .map({ (number) -> String in
                return String(number)
            })
            .subscribe(onNext: { (value) in
                if value == "0" {
                    self.tabBarController?.tabBar.items?[1].badgeValue = nil
                } else {
                    self.tabBarController?.tabBar.items?[1].badgeValue = value
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    fileprivate func setUpTableView() {
        tableView.delegate   = self
        tableView.dataSource = self
        tabBarController?.delegate = self
        tableView.register(UINib(nibName: PaginationTableViewCell.typeName, bundle: nil) , forCellReuseIdentifier: PaginationTableViewCell.typeName)
    }
    
    private func setRefreshControl() {
        refresh.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        tableView.addSubview(refresh)
    }
    
    private func setNavigationButtons() {
        navigationController?.navigationBar.largeTitleTextAttributes = nil
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.red,
                                                                   NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .headline)]
    }
    
    @objc private func createNewNews() {
        
    }
    
}
