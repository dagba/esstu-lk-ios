import Foundation
import RxSwift

class NewsViewModel: BaseViewModel {
    let disposeBag  = DisposeBag()
    let array       = Variable([Dialog]())
    let sortedArray = Variable([Dialog]())
    let badgeValue  = Variable(0)
    let offset      = Variable<Int>(0)
    var searchDialogs: [Dialog]?
    var count: Int {
        if isPaginationCellDeleted {
            return sortedArray.value.count
        } else {
            return sortedArray.value.count == 0 ? sortedArray.value.count : sortedArray.value.count + 1

        }
    }
    var isPaginationCellDeleted = false
    
    override init() {
        super.init()
        
        array.asObservable()
            .bind(to: sortedArray)
            .disposed(by: disposeBag)
        
    }
    
    func fetchAnnouncements() {
        offset.value = 0
        isLoading.value = true
        array.value.removeAll()
        APIManager.shared.rx_fetchAnnouncements()
            .subscribe(onNext: { [weak self] (announcements) in
                if (announcements.count % NetworkKey.offset) != 0 {
                    self?.isPaginationCellDeleted  = true
                } else {
                    self?.isPaginationCellDeleted  = false
                }
                self?.array.value = announcements
                /// slows down reloading
///                var count = 0
///                announcements.forEach({ (dialog) in
///                    count += dialog.unreadCount!
///                })
///                self?.badgeValue.value = 7 ///count
                self?.isLoading.value = false
                }, onError: { (error) in
                    self.alertMessage.onNext(error)
                    self.isLoading.value = false
            })
            .disposed(by: disposeBag)
    }
    
    func fetchAnnouncementsWithOffset() -> Observable<[Dialog]> {
        return APIManager.shared.rx_fetchAnnouncements(with: offset.value)
    }
    
    func filterList(with text: String) {
        let searchedText = text.lowercased()
        guard searchedText != "" else {
            if (array.value.count % NetworkKey.offset) != 0 {
                isPaginationCellDeleted  = true
            } else {
                isPaginationCellDeleted  = false
            }
            sortedArray.value = array.value
            isLoading.value = false /// reloads table view
            return
        }
        isLoading.value = true
        sortedArray.value.removeAll()
        if let dialogs = searchDialogs {
            isPaginationCellDeleted = true
            sortedArray.value = dialogs.filter { (dialog) -> Bool in
                let fullName = "\(dialog.user?.lastName ?? "") \(dialog.user?.firstName ?? "") \(dialog.user?.middleName ?? "")".lowercased()
                return dialog.chatDescription?.title?.lowercased().range(of: searchedText) != nil || dialog.user?.description?.lowercased().range(of: searchedText) != nil || fullName.range(of: searchedText) != nil
            }
            isLoading.value = false
        } else {
            APIManager.shared.rx_fetchAnnouncements(with: 0, limit: 999999999)
                .subscribe(onNext: { [weak self] (dialogs) in
                    self?.isPaginationCellDeleted = true
                    self?.searchDialogs = dialogs
                    
                    self?.sortedArray.value = dialogs.filter { (dialog) -> Bool in
                        let fullName = "\(dialog.user?.lastName ?? "") \(dialog.user?.firstName ?? "") \(dialog.user?.middleName ?? "")".lowercased()
                        return dialog.chatDescription?.title?.lowercased().range(of: searchedText) != nil || dialog.user?.description?.lowercased().range(of: searchedText) != nil || fullName.range(of: searchedText) != nil
                    }
                    
                    self?.isLoading.value = false
                    }, onError: { (error) in
                        self.alertMessage.onNext(error)
                        self.sortedArray.value = self.array.value
                        self.isLoading.value = false
                })
                .disposed(by: disposeBag)
        }
    }
    
    func doTransitionWith(_ segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case "AttachmentSegue":
            if let destinationVC = segue.destination as? AttachmentsViewController {
                if let row = (sender as? UIButton)?.tag, array.value.count >= 1 {
                    destinationVC.attachments = array.value[row].preview?.attachments
                }
                destinationVC.navigationItem.title = "Прикрепленные файлы"
            }
        default:
            break
        }
    }
    
}

