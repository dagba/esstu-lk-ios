import UIKit
import RxSwift
import RxCocoa
import UtilityKit
import Kingfisher

class RoomMapViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - UI properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapImageView: UIImageView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    // MARK: - UI Actions

    @IBAction func floorSwitched(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        guard let rooms = self.rooms else { return }
        guard rooms.count >= index else { return }
        setImage(for: rooms[index])
    }
    //MARK: - Properties
    private var disposeBag = DisposeBag()

    private var rooms: [Room]? {
        didSet {
            setUpNavigationBar()
        }
    }
    public var roomId: String? {
        didSet {
            roomId = roomId?.filter { Int(String($0)) != nil || $0 == "-" }
        }
    }
    
    private var week: String?
    let kfOptions: [KingfisherOptionsInfoItem] = [
        .transition(.fade(1)),
        .cacheOriginalImage
    ]
    
    //MARK: UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedControl.isEnabled = false
        segmentedControl.isHidden = true
        segmentedControl.removeAllSegments()
        week = navigationController?.navigationBar.topItem?.title
        navigationController?.navigationBar.topItem?.title = ""
        navigationController?.navigationBar.tintColor = .red
        setUpScrollView()
        setUpDoubeTapToZoom()
        setUpMapImageView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.topItem?.title = week
    }
    
    // MARK: - UIScrollView delegate methods

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mapImageView
    }
    
    // MARK: - Func definitions
    
    private func setUpScrollView() {
        scrollView?.delegate = self
        scrollView?.minimumZoomScale = 1.0
        scrollView?.maximumZoomScale = 3.0
    }
    
    private func setUpMapImageView() {
        mapImageView.contentMode = .scaleAspectFit
        view.makeToastActivity(.center)
        guard let roomId = roomId else { return showAlert(title: nil, message: "Некорректный идентификатор аудитории", completion: nil)}
        APIManager.shared.rx_fetchRoomMap(with: roomId)
            .subscribe(onNext: { [weak self] (rooms) in
                guard let `self` = self else { return }
                DispatchQueue.main.async {
                    self.segmentedControl.isEnabled = true
                    var counter = 0
                    rooms.forEach({ [weak self] room in
                        self?.segmentedControl.insertSegment(withTitle: String(room.floor), at: counter, animated: false)
                        counter += 1
                    })
                    if counter == 1 { self.segmentedControl.isHidden = true }
                    else { self.segmentedControl.isHidden = false }
                    self.segmentedControl.selectedSegmentIndex = 0
                }
                self.rooms = rooms
                guard let room = rooms.first else { return }

                self.setImage(for: room)
            }, onError: { (error) in
                self.view.hideToastActivity()
            })
            .disposed(by: disposeBag)
    }
    
    private func setUpDoubeTapToZoom() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapZoom(gesture:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    private func setImage(for room: Room, index: Int = 0) {
        guard let rooms = self.rooms else { return }
        guard rooms.count >= index else {
            showAlert(title: nil, message: "Ошибка сервера", completion: nil)
            return
        }
        let url = URL(string: NetworkKey.baseDownloadingURL + room.mapImageId)
        
        self.mapImageView.kf.setImage(with: url, placeholder: nil, options: self.kfOptions) { [weak self] result in
            switch result {
            case .success(let value):
                let image = value.image
                DispatchQueue.main.async {
                    self?.scrollView.contentSize = image.size
                    UIGraphicsBeginImageContext(image.size)
                    
                    let coordinates = room.coordinates
                    let context = UIGraphicsGetCurrentContext()!
                    context.setLineCap(CGLineCap.round)
                    context.setLineJoin(CGLineJoin.miter)
                    context.setAllowsAntialiasing(true)
                    context.setShouldAntialias(true)
                    context.setMiterLimit(2.0)
                    context.setStrokeColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4, alpha: 1))
                    context.setLineWidth(6.0)
                    
                    image.draw(at: CGPoint(x: 0.0, y: 0.0))
                    
                    for i in 0..<room.coordinates.count - 1 {
                        context.move(to: CGPoint(x: coordinates[i].x, y: coordinates[i].y))
                        context.addLine(to: CGPoint(x: coordinates[i + 1].x, y: coordinates[i + 1].y))
                    }
                    
                    context.strokePath()
                    if let locationMarkerImage = UIImage(named: "MapLocationMarker"), let lastPoint = room.coordinates.last {
                        let size = CGSize(width: locationMarkerImage.size.width*1.5, height: locationMarkerImage.size.height*1.5)
                        let drawRect = CGRect(
                            x: lastPoint.x - size.width/1.55,
                            y: -(lastPoint.y - size.height),
                            width: size.width,
                            height: size.height
                        )
                        context.translateBy(x: 0, y: size.height)
                        context.scaleBy(x: 1.0, y: -1.0)
                        context.draw(locationMarkerImage.cgImage!, in: drawRect)
                    }
                    
                    // Create new image
                    let newImage = UIGraphicsGetImageFromCurrentImageContext()
                    
                    // Tidy up
                    UIGraphicsEndImageContext();
                    
                    self?.mapImageView.image = newImage
                    self?.view.hideToastActivity()
                }
            case .failure(let error):
                self?.view.hideToastActivity()
                self?.showAlert(title: nil, message: error.localizedDescription, completion: nil)
            }}
    }
    
    @objc
    public func handleDoubleTapZoom(gesture: UITapGestureRecognizer) {
        if scrollView.zoomScale > scrollView.minimumZoomScale {
            scrollView.setZoomScale(scrollView.minimumZoomScale , animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale , animated: true)
        }
    }
    
    private func setUpNavigationBar() {
        navigationItem.backBarButtonItem?.title = ""
        navigationItem.title = "\(rooms?.last?.floor ?? 1) этаж \(roomId ?? "NaN") аудитория"
    }
    
}
