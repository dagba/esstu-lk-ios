import UIKit

class DayCell: UICollectionViewCell {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    public var list = [Subject]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    public var isInitialLaunch = true
}

/// MARK: - DataSource+Delegate methods
extension DayCell: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list.count == 0 && !isInitialLaunch { return 1 }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if list.count == 0 { return 370 }
        return 136
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if list.count == 0 && !isInitialLaunch {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyDayCell", for: indexPath)
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: SubjectTableViewCell.typeName, for: indexPath) as! SubjectTableViewCell
        
        cell.subject = list[indexPath.row]
        
        return cell
    }
}
