import UIKit
import RxSwift
import Kingfisher
import Material

class SubjectTableViewCell: UITableViewCell {
    private var disposeBag = DisposeBag()
    
    @IBOutlet private weak var beginTime        : UILabel!
    @IBOutlet private weak var endTime          : UILabel!
    @IBOutlet private weak var name             : UILabel!
    @IBOutlet private weak var teacher          : UILabel!
    @IBOutlet private weak var classroom        : UILabel!
    @IBOutlet private weak var timeLineView     : UIView!
    @IBOutlet private weak var typeOfSubject    : UILabel!
    @IBOutlet private weak var subjectTypeImage : UIImageView!
    
    private var room: String?
    
    @IBAction func locationTapped(_ sender: UIButton) {
        let roomMapViewController = Assembler.createRoomMapViewController()
        roomMapViewController.navigationItem.backBarButtonItem?.title = ""
        roomMapViewController.roomId = room
        
        UIViewController.getTopViewController()?.navigationController?.pushViewController(roomMapViewController, animated: true)
    }
    
    var subject: Subject? {
        didSet {
            name.font = Material.RobotoFont.medium(with: 16)
            name.text = subject?.name
            teacher.text = subject?.teacherInfo
            classroom.text = "а. \(subject?.classroomNumber ?? "")"
            room = subject?.classroomNumber
            if let sbj = subject {
                beginTime.text = lessonTime[sbj.lessonNumber!][0]
                endTime.text = lessonTime[sbj.lessonNumber!][1]
                switch sbj.lessonType ?? "" {
                case SubjectKey.labName:
                    subjectTypeImage.image = UIImage(named: "Lab")
                    typeOfSubject.text = "Лабораторная"
                case SubjectKey.lectureName:
                    subjectTypeImage.image = UIImage(named: "Lecture")
                    typeOfSubject.text = "Лекция"
                case SubjectKey.practiceName:
                     if sbj.weekNumber == nil {
                        subjectTypeImage.image = UIImage(named: "Fizra")
                        break
                     }
                    subjectTypeImage.image = UIImage(named: "Practice")
                    typeOfSubject.text = "Практика"
                default:
                    break
                }
            }
        }
    }

}

fileprivate let lessonTime = [["9:00","10:35"],
                          ["10:45","12:20"],
                          ["13:00","14:35"],
                          ["14:45","16:20"],
                          ["16:25","18:00"],
                          ["18:05","19:40"],
                          ["19:45","21:20"]]
