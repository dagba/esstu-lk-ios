import UIKit
import RxSwift
import RxCocoa
import BetterSegmentedControl
import Material

class ScheduleViewController: ViewController {
    public  var viewModel  = ScheduleViewModel()
    private let disposeBag = DisposeBag()
    
    /// MARK: - Properties and UI outlets
    public let refresh = UIRefreshControl()
    
    @IBOutlet private weak var weekSegmentedControl    : BetterSegmentedControl!
    @IBOutlet public  weak var weekDaySegmentedControl : BetterSegmentedControl!
    @IBOutlet private weak var refreshBarItem          : UIBarButtonItem!
    @IBOutlet private weak var collectionView          : UICollectionView! {
        didSet {
            collectionView.delegate   = self
            collectionView.dataSource = self
        }
    }
    
    /// MARK: - UI Actions
    
    /// MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "SCHEDULE_UPDATE") == nil {
            UserDefaults.standard.set(true, forKey: "SCHEDULE_UPDATE")
        }
        setCollectionView()
        setWeekSegmentedControl()
        setRefreshControl()
        setNavBarClear()
        setWeekDaySegmentedControl()
        bindViewModel()
        if !UserDefaults.standard.bool(forKey: "SCHEDULE_UPDATE") {
            viewModel.getLocalSchedule()
        } else {
            viewModel.getSchedule()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addBottomBorderToWeekDaySegmentedControl()
    }
    
}

/// MARK: - Func definition

extension ScheduleViewController {
    
    private func bindViewModel() {
        
        weekDaySegmentedControl.rx.controlEvent(.valueChanged)
            .subscribe(onNext: { (_) in
                let indexPath = IndexPath(item: Int(self.weekDaySegmentedControl.index), section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: [], animated: true)
            })
            .disposed(by: disposeBag)
        
        refreshBarItem.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.setCurrentDay()
                self?.viewModel.deleteSchedule()
                self?.viewModel.getSchedule()
            })
            .disposed(by: disposeBag)
        
        viewModel.sourceWeekNumber.asObservable()
            .bind { [weak self] (number) in
                let week = number == 0 ? "I" : "II"
                self?.navigationItem.title = week + " неделя"
                self?.viewModel.pickedWeekNumber.value = number
                self?.setCurrentDay()
            }
            .disposed(by: disposeBag)
        
        viewModel.pickedWeekNumber.asObservable()
            .bind { [weak self] (number) in
                self?.weekSegmentedControl.setIndex(UInt(number), animated: true)
                self?.viewModel.updateSortList(weekNumber: number)
                self?.viewModel.isInitialLaunch = false
                self?.collectionView.reloadData()
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .bind(onNext: { loading in
                loading ? self.view.makeToastActivity(.center) : self.view.hideToastActivity()
            })
            .disposed(by: disposeBag)
        
        viewModel.alertMessage
            .bind { [weak self] (error) in
                self?.showAlert(with: error)
                self?.refresh.endRefreshing()
            }
            .disposed(by: disposeBag)
        
        weekSegmentedControl.rx.controlEvent(.valueChanged)
            .subscribe({ [weak self] (_) in
                self?.viewModel.pickedWeekNumber.value = Int(self?.weekSegmentedControl.index ?? 0)
            })
            .disposed(by: disposeBag)
        
    }
    
    private func setWeekSegmentedControl() {
        weekSegmentedControl.segments =  LabelSegment.segments(withTitles: ["Первая", "Вторая"],
                                                               normalBackgroundColor: .white,
                                                               normalTextColor: .red,
                                                               selectedBackgroundColor: #colorLiteral(red: 0.2745098039, green: 0.4862745098, blue: 0.5647058824, alpha: 1),
                                                               selectedTextColor: .white)
    }
    
    fileprivate func setCollectionView() {
        collectionView.isPagingEnabled = true
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
        }
    }
    
    private func setCurrentDay() {
        var day = Calendar.current.component(.weekday, from: Date())
        let diff = day == 1 ? 1 : 2 /// Skips sunday
        day = day - diff
        weekDaySegmentedControl.setIndex(UInt(day))
        let weekDayTitles = ["Пн","Вт","Ср","Чт","Пт","Сб"]
        weekDaySegmentedControl.segments[day] = LabelSegment(text: weekDayTitles[day],
                                                   normalBackgroundColor: .clear,
                                                   normalFont: Material.RobotoFont.regular(with: 17),
                                                   normalTextColor: .red,
                                                   selectedBackgroundColor: .clear,
                                                   selectedFont: Material.RobotoFont.regular(with: 17),
                                                   selectedTextColor: .red)
    }

    private func setRefreshControl() {
        refresh.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
    }

    private func setWeekDaySegmentedControl() {
        let weekDayTitles = ["Пн","Вт","Ср","Чт","Пт","Сб"]
        weekDaySegmentedControl.segments = LabelSegment.segments(withTitles: weekDayTitles,
                                                       normalBackgroundColor: .clear,
                                                       normalFont: Material.RobotoFont.regular(with: 17),
                                                       normalTextColor: #colorLiteral(red: 0.2745098039, green: 0.4862745098, blue: 0.5647058824, alpha: 1),
                                                       selectedBackgroundColor: .clear,
                                                       selectedFont: Material.RobotoFont.regular(with: 17),
                                                       selectedTextColor: #colorLiteral(red: 0.2745098039, green: 0.4862745098, blue: 0.5647058824, alpha: 1))
        weekDaySegmentedControl.backgroundColor = .white
        weekDaySegmentedControl.alpha = 1.0
        weekDaySegmentedControl.layer.shadowColor = UIColor.black.cgColor
        weekDaySegmentedControl.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        weekDaySegmentedControl.layer.shadowRadius = 1.5
        weekDaySegmentedControl.layer.shadowOpacity = 0.2
        weekDaySegmentedControl.clipsToBounds = false
        weekDaySegmentedControl.layer.masksToBounds = false
    }
    
    fileprivate func addBottomBorderToWeekDaySegmentedControl() {
        weekDaySegmentedControl.backgroundColor = .white
        let separator = UIView(frame: CGRect(x: weekDaySegmentedControl.bounds.minX, y: weekDaySegmentedControl.bounds.height - 2, width: view.frame.width / 6, height: 2))
        separator.backgroundColor = .red
        weekDaySegmentedControl.indicatorViewInset = 0.0
        weekDaySegmentedControl.addSubviewToIndicator(separator)
    }
}
