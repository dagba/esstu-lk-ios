import Foundation
import RxSwift

class ScheduleViewModel: BaseViewModel {
    let disposeBag       = DisposeBag()
    let sourceList       = Variable([Subject]())
    let sortedList       = Variable([[Subject]]())
    let sourceWeekNumber = Variable<Int>(0)
    let pickedWeekNumber = Variable<Int>(0)
    var isInitialLaunch = true
    
    override init() {
        super.init()
        
    }
    
    public func updateSortList(weekNumber: Int) {
        self.sortedList.value.removeAll()
        for day in 0...6 {
            let dayList = sourceList.value
                                    .filter({ (subject) -> Bool in
                                        return (subject.weekNumber == weekNumber || subject.weekNumber == nil)
                                                && subject.dayNumber == day})
                                    .sorted { $0.lessonNumber ?? 0 < $1.lessonNumber ?? 0 }

            if dayList.count >= 2 {
                var bufferList = [Subject]()
                for lesson in dayList {
                    if !bufferList.contains(where: { $0.lessonNumber == lesson.lessonNumber }) {
                        bufferList.append(lesson)
                    }
                }
                self.sortedList.value.append(bufferList)
            } else {
                self.sortedList.value.append(dayList)
            }
        }
    }
    
    public func getStudyWeek() {
        APIManager.shared.rx_fetchStudyWeek()
            .subscribe(onNext: { [weak self] (weekNumber) in
                self?.sourceWeekNumber.value = weekNumber - 1
                }, onError: { [weak self] (error) in
                    self?.alertMessage.onNext(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func getSchedule() {
        isInitialLaunch = true
        isLoading.value = true
        APIManager.shared.rx_fetchSchedule()
            .subscribe(onNext: { [weak self] (list) in
//                for i in 0...6 {
//                    let asd = array.filter({ [weak self] (subject) -> Bool in
//                        return (subject.numberOfWeek == self?.pickedWeek.value || subject.numberOfWeek == nil)
//                            && subject.numberOfDay == i
//                    }).sorted { $0.numberOfLesson! < $1.numberOfLesson! }
//                    self?.sortList.value.append(asd)
//                }
                self?.sourceList.value = list
                self?.isLoading.value = false
                self?.getStudyWeek()
                }, onError: { [weak self] (error) in
                    self?.alertMessage.onNext(error)
                    self?.isLoading.value = false
                    self?.isInitialLaunch = false
            })
            .disposed(by: disposeBag)
    }
    
    public func getLocalSchedule() {
        isInitialLaunch = true
        isLoading.value = true
        APIManager.shared.rx_fetchScheduleFromLocal()
            .subscribe(onNext: { [weak self] (list) in
                self?.sourceList.value = list
                self?.isLoading.value = false
                self?.getStudyWeek()
                }, onError: { [weak self] (error) in
                    self?.alertMessage.onNext(error)
                    self?.isLoading.value = false
                    self?.isInitialLaunch = false
            })
            .disposed(by: disposeBag)
    }
    
    public func deleteSchedule() {
        APIManager.shared.deleteSchedulePersistent()
    }
    
}
