import UIKit

class GroupInfoTableViewCell: UITableViewCell {

    @IBOutlet public weak var nameLabel: UILabel!
    @IBOutlet public weak var usersCountLabel: UILabel!
    
    public var group: Group? {
        didSet {
            if let grp = group {
                nameLabel.text = grp.name
                usersCountLabel.text = "\(grp.participantsCount ?? 0) участников"
            }
        }
    }

}
