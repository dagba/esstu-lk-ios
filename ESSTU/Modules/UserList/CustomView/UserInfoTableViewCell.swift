import UIKit

class UserInfoTableViewCell: UITableViewCell {
    
    @IBOutlet public weak var fullNameLabel: UILabel!
    @IBOutlet public weak var descriptionLabel: UILabel!
    
    public var user: User? {
        didSet {
            if let usr = user {
                let fullName = "\(usr.lastName ?? "") \(usr.firstName ?? "") \(usr.middleName ?? "")"
                fullNameLabel.text = fullName
                descriptionLabel.text = usr.description
            }
        }
    }
    
}
