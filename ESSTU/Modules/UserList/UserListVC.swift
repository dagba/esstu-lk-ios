import UIKit
import RxCocoa
import RxSwift
//1148 - peerId of "WiFi" group
//1810 - peerId of "давай работай" group

class UserListViewController: ViewController {
    private let disposeBag = DisposeBag()
    public  var peerId: String?
    private var userList = [Any]()
    
    @IBOutlet weak var tableView: UITableView!
    
    /// MARK: - Lifecycle methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never
        setUpTableView()
        if let id = peerId {
            APIManager.shared.rx_fetchChatParticipants(peerId: id)
                .subscribe(onNext: { [weak self] (userList) in
                    self?.userList = userList
                    self?.tableView.reloadData()
                    }, onError: { [weak self] (error) in
                        self?.showAlert(with: error)
                })
                .disposed(by: disposeBag)
        }
    }
    
}

/// MARK: - Func definition

extension UserListViewController {

    private func setUpTableView() {
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }

}

/// MARK: - DataSource+Delegate methods

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let user = userList[indexPath.row] as? User {
            let cell = tableView.dequeueReusableCell(withIdentifier: UserInfoTableViewCell.typeName, for: indexPath) as! UserInfoTableViewCell
            cell.user = user
            return cell
        } else if let group = userList[indexPath.row] as? Group {
            let cell = tableView.dequeueReusableCell(withIdentifier: GroupInfoTableViewCell.typeName, for: indexPath) as! GroupInfoTableViewCell
            cell.group = group
            return cell
        }

        return UITableViewCell()
    }
}
