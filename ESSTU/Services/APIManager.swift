import Foundation
import Alamofire
import RxSwift
import ObjectMapper

typealias JSON = [String:Any]
typealias JSONArray = [[String:Any]]

class APIManager {
    static let shared = APIManager()
    private init() {
        self.accessToken = (UserDefaults.standard.value(forKey: "AccessToken") as? String) ?? ""
        self.userId = (UserDefaults.standard.value(forKey: "UserId") as? String) ?? ""
        self.userFullName = (UserDefaults.standard.value(forKey: "UserName") as? String) ?? ""
        self.userInfo = (UserDefaults.standard.value(forKey: "UserInfo") as? String) ?? ""
    }
    
    var accessToken = "" {
        didSet {
            UserDefaults.standard.set(accessToken, forKey: "AccessToken")
        }
    }
    var userId = "" {
        didSet {
            UserDefaults.standard.set(userId, forKey: "UserId")
        }
    }
    var userFullName = "" {
        didSet {
            UserDefaults.standard.set(userFullName, forKey: "UserName")
        }
    }
    var userInfo = "" {
        didSet {
            UserDefaults.standard.set(userInfo, forKey: "UserInfo")
        }
    }
    
    // MARK: - RX request methods
    func rx_authentication(username: String, password: String) -> Observable<MainUser> {
        return Observable<MainUser>.create({ (observer) -> Disposable in
            
            guard let url = URL(string: NetworkKey.authUrl) else { fatalError("Base auth url is invalide") }
            
            let parameters: Parameters = ["response_type" : "token",
                                          "grant_type"    : "password",
                                          "scope"         : "trust",
                                          "client_id"     : AuthKey.clientId,
                                          "client_secret" : AuthKey.clientSecret,
                                          "username"      : username,
                                          "password"      : password]
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil)
                .responseJSON { [weak self] (response) in
                    switch response.result {
                    case .success(let value):
                        if let user = Mapper<MainUser>().map(JSONObject: value), user.userId != nil {
                            self?.accessToken = user.accessToken ?? ""
                            self?.userId = user.userId ?? ""
                            let fullName = "\(user.lastName ?? "") \(user.firstName ?? "") \(user.middleName ?? "")"
                            self?.userFullName = fullName
                            self?.userId = user.userType == "EMPLOYEE" ?  "e" + (user.userId ?? "") : "s" + (user.userId ?? "")
                            observer.onNext(user)
                            observer.onCompleted()
                        } else {
                            if let errorDescription = value as? [String:String] {
                                observer.onError(APIError.wrongLoginPassword(errorDescription[AuthKey.errorDescription] ?? "Неправильный логин или пароль"))
                            } else {
                                observer.onError(APIError.wrongLoginPassword("Неуспешная сериализация\nВозможо проблемы с сервером\n Попробуйте перезайти в аккаунт"))
                            }
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create()
        })
    }
    
    public func rx_fetchScheduleFromLocal() -> Observable<[Subject]> {
        if !UserDefaults.standard.bool(forKey: "SCHEDULE_UPDATE") {
            if let subjects = self.getSubjectsFromLocal() {
                return Observable.just(subjects)
            } else {
                return Observable.error(APIError.mapFailed("Отсутствует расписание в локальной базе данных"))
            }
        }
        
        return Observable.empty()
    }
    
    func rx_fetchSchedule() -> Observable<[Subject]> {
        return Observable<[Subject]>.create({ [unowned self] (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v1/schedule/getSchedule2",
                              method: .get,
                              parameters: nil,
                              encoding: URLEncoding.httpBody,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let value = value as? JSONArray else {
                            observer.onError(APIError.mapFailed("Сессия устарела\nПерезайдите в аккаунт"))
                            observer.onCompleted()
                            return
                        }
                        let arrayOfSubject = Mapper<Subject>().mapArray(JSONArray: value)
                        if let error = self.saveSubjectsToLocal(value) {
                            observer.onError(error)
                        }
                        observer.onNext(arrayOfSubject)
                        observer.onCompleted()
                    case .failure(let error):
                        if let subjects = self.getSubjectsFromLocal() {
                            observer.onNext(subjects)
                        } else {
                            observer.onError(error)
                        }
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create()
        })
    }
    
    func rx_fetchStudyWeek() -> Observable<Int> {
        return Observable<Int>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v1/schedule/getStudyWeek",
                              method: .get,
                              parameters: nil,
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        if let number = value as? Int {
                            UserDefaults.standard.set(value, forKey: "WeekNumber")
                            observer.onNext(number)
                            observer.onCompleted()
                        } else {
                            if let weekNumber = UserDefaults.standard.value(forKey: "WeekNumber") as? Int {
                                observer.onNext(weekNumber)
                            }
                            observer.onError(APIError.gettingStudyWeekfailed("Не удалось получить номер текущей недели\n Проверьте на сайте"))
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        if let weekNumber = UserDefaults.standard.value(forKey: "WeekNumber") as? Int {
                            observer.onNext(weekNumber)
                        } else {
                            observer.onError(error)
                        }
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    // peerId: Int for CHAT, ANNOUNCEMENT. peerId: String for DIALOGUE
    func rx_fetchAnnouncements(with offset: Int = 0, limit: Int = NetworkKey.offset) -> Observable<[Dialog]> {
        return Observable<[Dialog]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/getDialogs",
                              method: .get,
                              parameters: ["type"   : DialogType.announce.rawValue,
                                           "limit"  : limit,
                                           "offset" : offset],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let tmp  = value as? JSON, let dialogsJson = tmp["dialogs"] as? JSONArray,
                              let tmp2 = value as? JSON, let previewMessagesJson = tmp2["messages"] as? JSONArray,
                              let tmp3 = value as? JSON, let usersJson = tmp3["users"] as? JSONArray,
                              let tmp4 = value as? JSON, let chatsJson = tmp4["chats"] as? JSONArray
                        else {
                            observer.onError(APIError.mapFailed("Сессия устарела\nПерезайдите в аккаунт"))
                            observer.onCompleted()
                            return
                        }
                        
                        var dialogs = Mapper<Dialog>().mapArray(JSONArray: dialogsJson)
                        let previewMessages = Mapper<Message>().mapArray(JSONArray: previewMessagesJson)
                        let users = Mapper<User>().mapArray(JSONArray: usersJson)
                        let chats = Mapper<ChatDescription>().mapArray(JSONArray: chatsJson)
                       
                        for dialog in dialogs {
                            dialog.preview = previewMessages.first(where: { (msg) -> Bool in
                                dialog.lastMessageId == msg.id
                            })
                            dialog.user = users.first(where: { (user) -> Bool in
                                return dialog.preview?.fromId == user.userId
                            })
                            dialog.chatDescription = chats.first(where: { (chat) -> Bool in
                                dialog.peerId == chat.id
                            })
                        }
                        dialogs.sort(by: { (first, second) -> Bool in
                            guard let a = first.lastMessageId, let b = second.lastMessageId else { return false }
                            return a > b
                        })
                        observer.onNext(dialogs)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    // peerId: Int for CHAT, ANNOUNCEMENT. peerId: String for DIALOGUE
    func rx_fetchConversations(with offset: Int = 0, limit: Int = NetworkKey.offset) -> Observable<[Dialog]> {
        return Observable<[Dialog]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/getDialogs",
                              method: .get,
                              parameters: ["type"   : DialogType.chat.rawValue,
                                           "limit"  : limit,
                                           "offset" : offset],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard   let tmp  = value as? JSON, let dialogsJson = tmp["dialogs"] as? JSONArray,
                                let tmp2 = value as? JSON, let previewMessagesJson = tmp2["messages"] as? JSONArray,
                                let tmp3 = value as? JSON, let usersJson = tmp3["users"] as? JSONArray,
                                let tmp4 = value as? JSON, let chatsJson = tmp4["chats"] as? JSONArray
                        else {
                                observer.onError(APIError.mapFailed("Сессия устарела\nПерезайдите в аккаунт"))
                                observer.onCompleted()
                                return
                        }
                        
                        var dialogs = Mapper<Dialog>().mapArray(JSONArray: dialogsJson)
                        let previewMessages = Mapper<Message>().mapArray(JSONArray: previewMessagesJson)
                        let users = Mapper<User>().mapArray(JSONArray: usersJson)
                 
                        let chats = Mapper<ChatDescription>().mapArray(JSONArray: chatsJson)
                        for dialog in dialogs {
                            dialog.preview = previewMessages.first(where: { (msg) -> Bool in
                                dialog.lastMessageId == msg.id
                            })
                            dialog.user = users.first(where: { (user) -> Bool in
                               return dialog.preview?.fromId == user.userId
                            })
                            dialog.chatDescription = chats.first(where: { (chat) -> Bool in
                                dialog.peerId == chat.id
                            })
                        }
                        dialogs.sort(by: { (first, second) -> Bool in
                            guard let a = first.lastMessageId, let b = second.lastMessageId else { return false }
                            return a > b
                        })
                        dialogs = dialogs.map { (dialog) -> Dialog in
                            if dialog.preview?.text == nil {
                                if let count = dialog.preview?.attachments?.count, count > 0 {
                                    dialog.preview?.text = "<Вложение>"
                                } else {
                                    dialog.preview?.text = ""
                                }
                            }
                            return dialog
                        }
                        observer.onNext(dialogs)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    // peerId: Int for CHAT, ANNOUNCEMENT. peerId: String for DIALOGUE
    func rx_fetchDialogs(with offset: Int = 0, limit: Int = NetworkKey.offset) -> Observable<[Dialog]> {
        return Observable<[Dialog]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/getDialogs",
                              method: .get,
                              parameters: ["type"   : DialogType.dialogue.rawValue,
                                           "limit"  : limit,
                                           "offset" : offset],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let tmp  = value as? JSON, let dialogsJson = tmp["dialogs"] as? JSONArray,
                            let tmp2 = value as? JSON, let previewMessagesJson = tmp2["messages"] as? JSONArray,
                            let tmp3 = value as? JSON, let usersJson = tmp3["users"] as? JSONArray
                            else {
                                observer.onError(APIError.mapFailed("Сессия устарела\nПерезайдите в аккаунт"))
                                observer.onCompleted()
                                return
                        }
                        
                        var dialogs = Mapper<Dialog>().mapArray(JSONArray: dialogsJson)
                        let previewMessages = Mapper<Message>().mapArray(JSONArray: previewMessagesJson)
                        let users = Mapper<User>().mapArray(JSONArray: usersJson)
         
                        for dialog in dialogs {
                            dialog.preview = previewMessages.first(where: { (msg) -> Bool in
                                dialog.lastMessageId == msg.id
                            })
                            dialog.user = users.first(where: { (user) -> Bool in
                                dialog.preview?.fromId == user.userId
                            })

                            let peerUser = users.first(where: { (user) -> Bool in
                                return dialog.preview?.peerId == user.userId
                            })
                            dialog.preview?.peerUser = peerUser
                            dialog.preview?.fromUser = dialog.user
                        }
                        dialogs.sort(by: { (first, second) -> Bool in
                            guard let a = first.lastMessageId, let b = second.lastMessageId else { return false }
                            return a > b
                        })
                        dialogs = dialogs.map { (dialog) -> Dialog in
                            if dialog.preview?.text == nil {
                                if let count = dialog.preview?.attachments?.count, count > 0 {
                                    dialog.preview?.text = "<Вложение>"
                                } else {
                                    dialog.preview?.text = ""
                                }
                            }
                            return dialog
                        }
                        observer.onNext(dialogs)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_fetchChatLog(peerId: String, offset: Int = 0, limit: Int = 999999) -> Observable<[Message]> {
        return Observable<[Message]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/getHistory",
                              method: .get,
                              parameters: ["peerId" : peerId,
                                           "offset" : offset,
                                           "limit"  : limit],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let tmp2 = value as? JSON, let previewMessagesJson = tmp2["messages"] as? JSONArray,
                            let tmp3 = value as? JSON, let usersJson = tmp3["users"] as? JSONArray
                            else {
                                observer.onError(APIError.mapFailed("Сессия устарела\nПерезайдите в аккаунт"))
                                observer.onCompleted()
                                return
                        }
                        
                        var messages = Mapper<Message>().mapArray(JSONArray: previewMessagesJson)
                        let users = Mapper<User>().mapArray(JSONArray: usersJson)
                        for i in 0..<messages.count {
                            messages[i].fromUser = users.first(where: { (user) -> Bool in
                                user.userId == messages[i].fromId
                            })
                        }
                        messages.sort { $0.date! > $1.date! }
                        observer.onNext(messages)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_markAsRead(peer: String, maxId: Int) {
        Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/readHistory",
                          method: .post,
                          parameters: ["peer"  : peer,
                                       "maxId" : maxId],
                          encoding: JSONEncoding.default,
                          headers: ["Authorization" : "Bearer \(self.accessToken)"])
            .responseJSON(completionHandler: { (response) in
                
            })
    }
    
    func rx_fetchUser(id: String) -> Observable<User> {
        return Observable<User>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v1/users/getUser",
                              method: .get,
                              parameters: ["id" : id],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        
                        if let user = Mapper<User>().map(JSON: value as! JSON) {
                            observer.onNext(user)
                            observer.onCompleted()
                        } else {
                            observer.onError(APIError.mapFailed("Ошибка сервера"))
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_fetchChatParticipants(peerId: String) -> Observable<[Any]> {
        return Observable<[Any]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/getChatFull",
                              method: .get,
                              parameters: ["id" : peerId],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        
                        if let chat = value as? JSON, let userList = chat["users"] as? JSONArray {
                            var users = Mapper<User>().mapArray(JSONArray: userList)
                            var groups = Mapper<Group>().mapArray(JSONArray: userList)
                            var genericList = [Any]()
                            for i in 0..<users.count {
                                if users[i].firstName != nil {
                                    genericList.append(users[i] as Any)
                                }
                            }
                            for i in 0..<groups.count {
                                if groups[i].name != nil {
                                    genericList.append(groups[i] as Any)
                                }
                            }
                         
                            observer.onNext(genericList)
                            observer.onCompleted()
                        } else {
                            observer.onError(APIError.mapFailed("Ошибка сервера"))
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_sendMessage(peerId: String, text: String?, attachments: [String] = []) -> Observable<Int> {
        return Observable<Int>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/sendMessage",
                              method: .post,
                              parameters: ["peer" : peerId,
                                           "message" : text ?? NSNull(),
                                           "attachments"  : attachments],
                              encoding: JSONEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        observer.onNext(123)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_deleteUserFromDialog(peerId: String) -> Observable<Bool> {
        return Observable<Bool>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/removeChatUser",
                              method: .post,
                              parameters: ["chatId" : peerId,
                                           "user"   : self.userId],
                              encoding: JSONEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        if let state = value as? Bool {
                            observer.onNext(state)
                            observer.onCompleted()
                        } else {
                            observer.onError(APIError.mapFailed("Удаление чата произошло неудачно\nОшибка сервера: \(value)"))
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func rx_uploadFile(_ data: UIImage, name: String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(data.jpegData(compressionQuality: 0.5) ?? Data(), withName: "file", fileName: name, mimeType: "image/jpeg")
                
            }, to: NetworkKey.baseUrl + "/api/v1/file/uploadFile",
               method: .post,
               headers: ["Authorization" : "Bearer \(self.accessToken)"]) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let json = response.value as! JSON
                        if let code = json["code"] as? String {
                            print("Code of the uploaded file - \(code)")
                            observer.onNext(code)
                            observer.onCompleted()
                        } else {
                            observer.onError(APIError.mapFailed("Ошибка при загрузке изображения"))
                            observer.onCompleted()
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        })
    }
    
    // FIXME
    func rx_createNewChat(_ dialog: Dialog, participants: [String]) -> Observable<Dialog> {
        return Observable<Dialog>.create({ (observer) -> Disposable in
            let attachmentsFileId = dialog.preview?.attachments?.reduce([String]()) { (result, attachement) -> [String] in
                var tmpResult = result
                tmpResult.append(attachement.fileId!)
                return tmpResult
            }
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v2/messenger/createChat",
                              method: .post,
                              parameters: ["type"       : dialog.type!,
                                           "name"       : dialog.chatDescription!.title!,
                                           "users"      : participants,
                                           "message"    : dialog.preview!.text!,
                                           "attachments": attachmentsFileId!,
                                           "context"    : ""],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let dialog = Mapper<Dialog>().map(JSON: value as! [String:Any]) else {
                            observer.onError(APIError.mapFailed("Не удалось создать чат\n Возможно проблемы с сервером"))
                            observer.onCompleted()
                            return
                        }
                        observer.onNext(dialog)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    public func rx_registerFCMWith(token: String) -> Observable<Bool> {
        return Observable<Bool>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.baseUrl + "/api/v1/auth/tokenRegistry",
                              method: .post,
                              parameters: ["token" : token],
                              encoding: JSONEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        observer.onNext(true)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
    func downloadFileWith(url: String, fileName: String) -> Observable<URL> {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent(fileName)
            return (documentsURL, [.removePreviousFile])
        }
        return Observable<URL>.create({ (observer) -> Disposable in
            
            Alamofire.download(url, to: destination).responseData { response in
                switch response.result {
                case .success:
                    let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let destinationFileUrl = documentsUrl.appendingPathComponent(fileName)
                  
                    observer.onNext(destinationFileUrl)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        })
    }
    
    public func rx_fetchRoomMap(with number: String) -> Observable<[Room]> {
        return Observable<[Room]>.create({ (observer) -> Disposable in
            
            Alamofire.request(NetworkKey.mapUrl + "/api/v1/search/room",
                              method: .get,
                              parameters: ["q" : number],
                              encoding: URLEncoding.default,
                              headers: ["Authorization" : "Bearer \(self.accessToken)"])
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        guard let json = value as? JSONArray else {
                            observer.onError(APIError.mapFailed("Ошибка сервера"))
                            observer.onCompleted()
                            return
                        }
                        let rooms = Mapper<Room>().mapArray(JSONArray: json)
                        observer.onNext(rooms)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        })
    }
    
//    public func removeFromFirebase() {
//        Alamofire.request(NetworkKey.authUrl + "/delete",
//                          method: .get,
//                          parameters: ["q" : number],
//                          encoding: URLEncoding.default,
//                          headers: ["Authorization" : "Bearer \(self.accessToken)"])
//            .responseJSON(completionHandler: { (response) in
//                switch response.result {
//                case .success(let value):
//                    guard let json = value as? JSONArray else {
//                        observer.onError(APIError.mapFailed("Ошибка сервера"))
//                        observer.onCompleted()
//                        return
//                    }
//                    let rooms = Mapper<Room>().mapArray(JSONArray: json)
//                    observer.onNext(rooms)
//                    observer.onCompleted()
//                case .failure(let error):
//                    observer.onError(error)
//                    observer.onCompleted()
//                }
//            })
//    }
    
    // MARK: - Functions
    
    func logout() {
        self.accessToken = ""
        self.userId = ""
        self.userFullName = ""
        self.userInfo = ""
        UserDefaults.standard.removeObject(forKey: "AccessToken")
        UserDefaults.standard.removeObject(forKey: "UserId")
        UserDefaults.standard.removeObject(forKey: "WeekNumber")
        UserDefaults.standard.removeObject(forKey: "UserName")
        UserDefaults.standard.removeObject(forKey: "SCHEDULE_UPDATE")
        if let fileURL = subjectsFileUrl {
            try? FileManager.default.removeItem(at: fileURL)
        }
    }
    
    func deleteSchedulePersistent() {
        if let fileURL = subjectsFileUrl {
            try? FileManager.default.removeItem(at: fileURL)
        }
    }
    
    var subjectsFileUrl: URL? {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Subjects.json")
        return fileUrl
    }
    
    func saveSubjectsToLocal(_ json: Any) -> Error? {
        guard let fileURL = subjectsFileUrl else { return APIError.mapFailed("Не удалось сохранить расписание на устройство") }
            
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            try data.write(to: fileURL, options: [])
        } catch {
            print("\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!SAVE JSON TO FILE FAILURE!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n")
            return error
        }
        
        return nil
    }
    
    func getSubjectsFromLocal() -> [Subject]? {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("Subjects.json")
        // mock schedule from json file "schedule2.json"
//        guard let fileUrl = getScheduleFileUrl() else { return [] }
        
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let subjects = try JSONSerialization.jsonObject(with: data, options: []) as? JSONArray else { return nil }
            return Mapper<Subject>().mapArray(JSONArray: subjects)
        } catch {
            return nil
        }
    }
    
    func transformFromJSON(_ value: Any?) -> String? {
        if let size = value as? Int {
            if size < 1024 {
                return String(size) + " B"
            }
            if size/1024 > 1000 {
                if ((size/1024)/1024) > 1000 {
                    return "\(size/1024/1024/1024) GB"
                }
                return "\(size/1024/1024) MB"
            } else {
                return "\(size/1024) KB"
            }
        }
        return nil
    }
    
    func getScheduleFileUrl() -> URL? {
        let fileName = "schedule2"
        if let fileUrl = Bundle.main.url(forResource: fileName, withExtension: "json") {
            return fileUrl
        }
        
        return nil
    }
}

public enum APIError: Error {
    case wrongLoginPassword(String)
    case wrongAuthUrl(String)
    case mapFailed(String)
    case gettingStudyWeekfailed(String)
}

struct AuthKey {
    static let clientId         = "personal_office_ios2"
    static let clientSecret     = "c11120eda0ca0d0d689e2280f8632d10996f30fed3a497d72095a019bd72c1c7"
    static let errorDescription = "error_description"
}

public struct NetworkKey {
    static let authUrl = "https://esstu.ru/auth/oauth/token"
//    static let baseUrl = "https://esstu.ru/mlk"
    static let baseUrl = "https://esstu.ru/lk"
    static let mapUrl = "https://esstu.ru/ror"
    static let offset = 10
    static let baseDownloadingURL = "https://esstu.ru/aicstorages/publicDownload/"
}

