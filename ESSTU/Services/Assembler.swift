import UIKit

class Assembler {
    
    static public func createMainViewController() -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: MainTabBarViewController.typeName)
    }
    
    static public func createChatViewController() -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ChatViewController.typeName)
    }
    
    static public func createUsersListViewController() -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UserListViewController.typeName)
    }
    
    static public func createAuthViewController() -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: AuthViewController.typeName)
    }
    
    static public func createRoomMapViewController() -> RoomMapViewController {
        return RoomMapViewController(nibName: RoomMapViewController.typeName, bundle: nil)
    }
    
}
